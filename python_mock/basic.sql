/*
 Navicat Premium Data Transfer

 Source Server         : 71的数据库
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : fanmao71

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 04/11/2021 13:25:27
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for emps
-- ----------------------------
DROP TABLE IF EXISTS `emps`;
CREATE TABLE `emps`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名字',
  `salary` decimal(10, 2) NOT NULL COMMENT '月薪',
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位',
  `join_in` datetime(0) NULL DEFAULT NULL COMMENT '入职日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
