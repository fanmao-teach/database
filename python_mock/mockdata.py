import yaml
import pymysql
from faker import Faker

def config():
    with open('./config.yaml', encoding='utf8') as file:
        return yaml.safe_load(file)
print(config(),type(config()))

class MockData:
    def __init__(self):
        """
        读取yaml配置文件
        """
        data = config()
        self.connection = pymysql.connect(
            host=data['host'],
            user=data['username'],
            password=str(data['password']),
            database='blogs',
            cursorclass=pymysql.cursors.DictCursor)
        self.fk = Faker("zh_CN")

    def insert_data(self,count):
        for _ in range(count):
            with self.connection.cursor() as cursor:
                sql = "INSERT INTO `blogs`.`t_blog` (`title`, `author`, `content`) VALUES (%s, %s, %s)"
                cursor.execute(sql,(self.fk.name(),self.fk.name(),self.fk.text()))
            self.connection.commit()

if __name__ == '__main__':
    mk = MockData()
    mk.insert_data(1000)
