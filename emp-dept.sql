/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云-55
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : homework

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 08/07/2021 15:34:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `manager` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `dno`(`dno`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, 'D001', '研发部', '张有为');
INSERT INTO `dept` VALUES (2, 'K001', '客服部', '王小美');
INSERT INTO `dept` VALUES (3, 'X001', '销售部', '张大拿');

SET FOREIGN_KEY_CHECKS = 1;




SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eno` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salary` decimal(10, 2) NOT NULL,
  `dno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `e-dno`(`dno`) USING BTREE,
  CONSTRAINT `e-dno` FOREIGN KEY (`dno`) REFERENCES `dept` (`dno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES (1, 'E001', '王喜', 8000.00, 'D001');
INSERT INTO `emp` VALUES (2, 'E002', '张行行', 10000.00, 'K001');
INSERT INTO `emp` VALUES (3, 'E003', '李美', 9000.00, 'K001');
INSERT INTO `emp` VALUES (4, 'E004', '黄滴', 7000.00, 'X001');
INSERT INTO `emp` VALUES (5, 'E005', '小玲', 19000.00, 'K001');
INSERT INTO `emp` VALUES (6, 'E006', '张玲', 11000.00, 'X001');
INSERT INTO `emp` VALUES (7, 'E007', '王玲', 20000.00, 'X001');
INSERT INTO `emp` VALUES (8, 'E008', '赵玲', 15000.00, 'D001');
INSERT INTO `emp` VALUES (9, 'E009', '王大大', 11000.00, 'D001');
INSERT INTO `emp` VALUES (10, 'E010', '张莉', 12000.00, 'K001');

SET FOREIGN_KEY_CHECKS = 1;
