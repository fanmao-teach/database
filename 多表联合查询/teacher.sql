/*
 Navicat Premium Data Transfer

 Source Server         : mysql-fanmao60
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : fanmao60

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/08/2021 08:58:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '教师名',
  `age` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of teacher
-- ----------------------------
INSERT INTO `teacher` VALUES (1, 'Tom', 29, '420117202006040000', 'tom@qq.com');
INSERT INTO `teacher` VALUES (2, 'Jack', 30, '420117202006041111', 'jack@qq.com');
INSERT INTO `teacher` VALUES (3, 'Mary', 31, '420117202006042222', 'mary@qq.com');
INSERT INTO `teacher` VALUES (4, 'Timo', 35, '420117202006043333', 'timo@qq.com');
INSERT INTO `teacher` VALUES (5, 'Faker', 38, '420117202006044444', 'faker@qq.com');
INSERT INTO `teacher` VALUES (6, 'Bob', 35, '420117202006045555', 'bob@qq.com');
INSERT INTO `teacher` VALUES (7, 'kelly', 40, '420117202006046666', 'kelly@qq.com');
INSERT INTO `teacher` VALUES (8, 'Rose', 42, '420117202006047777', 'rose@qq.com');
INSERT INTO `teacher` VALUES (9, 'Hale', 55, '420117202006048888', 'hale@qq.com');
INSERT INTO `teacher` VALUES (10, 'John', 49, '420117202006049999', 'john@qq.com');
INSERT INTO `teacher` VALUES (11, 'Amy', 55, '42011720200604888X', 'amy@qq.com');
INSERT INTO `teacher` VALUES (12, 'Judy', 49, '42011720200604999X', 'judy@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
