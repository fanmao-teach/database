/*
 Navicat Premium Data Transfer

 Source Server         : mysql-fanmao60
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : fanmao60

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/08/2021 08:59:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for student_course
-- ----------------------------
DROP TABLE IF EXISTS `student_course`;
CREATE TABLE `student_course`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `course_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student_course
-- ----------------------------
INSERT INTO `student_course` VALUES (1, 1, 1);
INSERT INTO `student_course` VALUES (2, 1, 2);
INSERT INTO `student_course` VALUES (3, 2, 3);
INSERT INTO `student_course` VALUES (4, 2, 4);
INSERT INTO `student_course` VALUES (5, 3, 5);
INSERT INTO `student_course` VALUES (6, 3, 6);
INSERT INTO `student_course` VALUES (7, 4, 7);
INSERT INTO `student_course` VALUES (8, 4, 8);
INSERT INTO `student_course` VALUES (9, 5, 9);
INSERT INTO `student_course` VALUES (10, 5, 10);
INSERT INTO `student_course` VALUES (11, 6, 1);
INSERT INTO `student_course` VALUES (12, 6, 2);
INSERT INTO `student_course` VALUES (13, 7, 3);
INSERT INTO `student_course` VALUES (14, 7, 4);

SET FOREIGN_KEY_CHECKS = 1;
