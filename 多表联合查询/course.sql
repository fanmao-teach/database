/*
 Navicat Premium Data Transfer

 Source Server         : mysql-fanmao60
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : fanmao60

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/08/2021 08:58:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `course_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `teacher_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, '高等数学', 1);
INSERT INTO `course` VALUES (2, '英语', 2);
INSERT INTO `course` VALUES (3, '政治', 3);
INSERT INTO `course` VALUES (4, '信息论', 4);
INSERT INTO `course` VALUES (5, '数据结构和算法', 5);
INSERT INTO `course` VALUES (6, '体育', 6);
INSERT INTO `course` VALUES (7, '模拟电路', 7);
INSERT INTO `course` VALUES (8, '数字电路', 8);
INSERT INTO `course` VALUES (9, '通信原理', 9);
INSERT INTO `course` VALUES (10, '信号系统', 10);
INSERT INTO `course` VALUES (11, '概率论', 13);
INSERT INTO `course` VALUES (12, '光学原理', 14);

SET FOREIGN_KEY_CHECKS = 1;
