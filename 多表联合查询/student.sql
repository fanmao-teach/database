/*
 Navicat Premium Data Transfer

 Source Server         : mysql-fanmao60
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : fanmao60

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 03/08/2021 08:58:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '无名',
  `age` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `id_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '赵小明', 21, '420117199303036666');
INSERT INTO `student` VALUES (2, '王小红', 22, '420117199303037777');
INSERT INTO `student` VALUES (3, '张小虎', 18, '420117199303038888');
INSERT INTO `student` VALUES (4, '李小平', 19, '420117199303039999');
INSERT INTO `student` VALUES (5, '刘美丽', 21, '420117199303035555');
INSERT INTO `student` VALUES (6, '周杰', 22, '420117199303034444');
INSERT INTO `student` VALUES (7, '秦小贤', 19, '420117199303033333');
INSERT INTO `student` VALUES (8, '马笑', 23, '420117199303032222');
INSERT INTO `student` VALUES (9, '艾伦', 22, '420117199303031111');
INSERT INTO `student` VALUES (10, '包小天', 20, '420117199303030000');

SET FOREIGN_KEY_CHECKS = 1;
