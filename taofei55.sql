/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云-55
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : taofei55

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 08/07/2021 09:18:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for checkin
-- ----------------------------
DROP TABLE IF EXISTS `checkin`;
CREATE TABLE `checkin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名',
  `checkintime` datetime(0) NULL DEFAULT NULL COMMENT '签到时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of checkin
-- ----------------------------
INSERT INTO `checkin` VALUES (10, '赵云', '2021-07-01 11:47:44');
INSERT INTO `checkin` VALUES (11, '李贝', '2021-07-02 11:48:02');
INSERT INTO `checkin` VALUES (12, '赵云', '2021-07-02 11:48:17');
INSERT INTO `checkin` VALUES (13, '张闯', '2021-07-03 11:58:34');
INSERT INTO `checkin` VALUES (14, '李备', '2021-07-01 14:12:59');
INSERT INTO `checkin` VALUES (15, '赵云', '2021-07-01 14:13:10');
INSERT INTO `checkin` VALUES (16, '张闯', '2021-07-03 10:08:31');
INSERT INTO `checkin` VALUES (17, '张闯', '2021-07-03 10:08:39');
INSERT INTO `checkin` VALUES (18, '张闯', '2021-07-07 11:52:05');
INSERT INTO `checkin` VALUES (19, '赵云', '2021-07-07 11:52:17');
INSERT INTO `checkin` VALUES (20, '王湾', '2021-07-07 11:52:38');
INSERT INTO `checkin` VALUES (21, '李贝', '2021-06-28 11:52:56');
INSERT INTO `checkin` VALUES (22, '李贝', '2021-06-29 11:53:12');
INSERT INTO `checkin` VALUES (23, '李贝', '2021-06-30 11:53:22');
INSERT INTO `checkin` VALUES (24, '李贝', '2021-06-30 11:53:30');
INSERT INTO `checkin` VALUES (25, '李贝', '2021-06-30 11:53:38');
INSERT INTO `checkin` VALUES (26, '李贝', '2019-07-11 13:49:07');

-- ----------------------------
-- Table structure for depts
-- ----------------------------
DROP TABLE IF EXISTS `depts`;
CREATE TABLE `depts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) NOT NULL COMMENT '部门id',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of depts
-- ----------------------------
INSERT INTO `depts` VALUES (1, 100, '研发');
INSERT INTO `depts` VALUES (2, 200, '销售');
INSERT INTO `depts` VALUES (3, 300, '客服');

-- ----------------------------
-- Table structure for emps
-- ----------------------------
DROP TABLE IF EXISTS `emps`;
CREATE TABLE `emps`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL COMMENT '工号',
  `emp_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工姓名',
  `dept_id` int(11) NOT NULL COMMENT '部门id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emps
-- ----------------------------
INSERT INTO `emps` VALUES (1, 5501, '王云', 100);
INSERT INTO `emps` VALUES (2, 5502, '张五忌', 100);
INSERT INTO `emps` VALUES (3, 5503, '曹德望', 200);
INSERT INTO `emps` VALUES (4, 5504, '张语', 300);
INSERT INTO `emps` VALUES (5, 5505, '鲁班', 200);
INSERT INTO `emps` VALUES (6, 5506, '姜子牙', 300);
INSERT INTO `emps` VALUES (7, 5507, '周王', 100);
INSERT INTO `emps` VALUES (8, 5508, '秦王', 200);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单号',
  `userid` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名字',
  `pay` int(10) NOT NULL COMMENT '订单金额',
  `ordertime` datetime(0) NOT NULL COMMENT '订单时间',
  `products` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 'A001', '张山', 100, '2021-07-03 14:08:03', '苹果');
INSERT INTO `orders` VALUES (2, 'A002', '里斯', 300, '2021-07-03 14:08:36', '啤酒');
INSERT INTO `orders` VALUES (3, 'A001', '张山', 200, '2021-07-03 14:09:25', '苹果');
INSERT INTO `orders` VALUES (4, 'V003', '李五', 100, '2021-07-03 14:10:09', '苹果');
INSERT INTO `orders` VALUES (5, 'V002', '小小', 800, '2021-07-03 14:10:51', '小米');
INSERT INTO `orders` VALUES (6, 'V002', '小小', 500, '2021-07-03 14:20:15', '啤酒');
INSERT INTO `orders` VALUES (7, 'V003', '李五', 1200, '2021-07-05 10:45:51', '香蕉');

-- ----------------------------
-- Table structure for students
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学生姓名',
  `course` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '科目',
  `score` int(10) NULL DEFAULT NULL COMMENT '成绩',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of students
-- ----------------------------
INSERT INTO `students` VALUES (1, '林冲', '体育', 100, '男', 30);
INSERT INTO `students` VALUES (2, '武大郎', '语文', 80, '男', 25);
INSERT INTO `students` VALUES (3, '武松', '体育', 100, '男', 23);
INSERT INTO `students` VALUES (4, '嫦娥', '体育', 40, '女', 18);
INSERT INTO `students` VALUES (5, '大乔', '体育', 70, '女', 19);
INSERT INTO `students` VALUES (6, '小乔', '体育', 60, '女', 18);
INSERT INTO `students` VALUES (7, '林冲', '语文', 100, '男', 30);
INSERT INTO `students` VALUES (8, '林冲', '数学', 60, '男', 30);
INSERT INTO `students` VALUES (9, '武大郎', '体育', 90, '男', 25);
INSERT INTO `students` VALUES (10, '武大郎', '数学', 90, '男', 25);
INSERT INTO `students` VALUES (11, '武松', '语文', 30, '男', 23);
INSERT INTO `students` VALUES (12, '武松', '数学', 60, '男', 23);
INSERT INTO `students` VALUES (13, '嫦娥', '语文', 100, '女', 18);
INSERT INTO `students` VALUES (14, '嫦娥', '数学', 100, '女', 18);
INSERT INTO `students` VALUES (15, '大乔', '语文', 80, '女', 19);
INSERT INTO `students` VALUES (16, '大乔', '数学', 90, '女', 19);
INSERT INTO `students` VALUES (17, '小乔', '语文', 50, '女', 18);
INSERT INTO `students` VALUES (18, '小乔', '数学', 90, '女', 18);
INSERT INTO `students` VALUES (20, '王大仙', NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
