/*
 Navicat Premium Data Transfer

 Source Server         : 测试服务器
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : 1.117.45.85:3306
 Source Schema         : huahua_0000

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 08/07/2021 16:43:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `cid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cid`(`cid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, 'C01', '语文');
INSERT INTO `course` VALUES (2, 'C02', '数学');
INSERT INTO `course` VALUES (3, 'C03', '英语');

-- ----------------------------
-- Table structure for sc
-- ----------------------------
DROP TABLE IF EXISTS `sc`;
CREATE TABLE `sc`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `cid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `score` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sc-sid`(`sid`) USING BTREE,
  INDEX `sc-cid`(`cid`) USING BTREE,
  CONSTRAINT `sc-cid` FOREIGN KEY (`cid`) REFERENCES `course` (`cid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `sc-sid` FOREIGN KEY (`sid`) REFERENCES `student` (`sid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sc
-- ----------------------------
INSERT INTO `sc` VALUES (1, 'S001', 'C01', 40);
INSERT INTO `sc` VALUES (2, 'S001', 'C02', 60);
INSERT INTO `sc` VALUES (3, 'S001', 'C03', 80);
INSERT INTO `sc` VALUES (4, 'S002', 'C01', 90);
INSERT INTO `sc` VALUES (5, 'S002', 'C02', 80);
INSERT INTO `sc` VALUES (6, 'S002', 'C03', 90);
INSERT INTO `sc` VALUES (7, 'S003', 'C01', 60);
INSERT INTO `sc` VALUES (8, 'S003', 'C02', 20);
INSERT INTO `sc` VALUES (9, 'S003', 'C03', 30);
INSERT INTO `sc` VALUES (10, 'S004', 'C01', 45);
INSERT INTO `sc` VALUES (11, 'S004', 'C02', 70);
INSERT INTO `sc` VALUES (12, 'S004', 'C03', 80);
INSERT INTO `sc` VALUES (13, 'S005', 'C01', 10);
INSERT INTO `sc` VALUES (14, 'S005', 'C02', 80);
INSERT INTO `sc` VALUES (15, 'S005', 'C03', 40);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sid` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` int NOT NULL,
  `sex` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sid`(`sid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 'S001', '张山', 19, '男');
INSERT INTO `student` VALUES (2, 'S002', '小亮', 25, '男');
INSERT INTO `student` VALUES (3, 'S003', '张妹妹', 20, '女');
INSERT INTO `student` VALUES (4, 'S004', '王小美', 23, '女');
INSERT INTO `student` VALUES (5, 'S005', '张亮', 24, '男');

SET FOREIGN_KEY_CHECKS = 1;
