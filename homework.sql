/*
 Navicat Premium Data Transfer

 Source Server         : 阿里云-55
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : rm-bp188nr95fk4l9545ao.mysql.rds.aliyuncs.com:3306
 Source Schema         : homework

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 08/07/2021 09:18:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for checkin
-- ----------------------------
DROP TABLE IF EXISTS `checkin`;
CREATE TABLE `checkin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `checktime` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `chckinno`(`sno`) USING BTREE,
  CONSTRAINT `chckinno` FOREIGN KEY (`sno`) REFERENCES `student` (`sno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of checkin
-- ----------------------------
INSERT INTO `checkin` VALUES (2, '5501', '2021-07-05');
INSERT INTO `checkin` VALUES (3, '5501', '2021-07-04');
INSERT INTO `checkin` VALUES (4, '5502', '2021-07-05');
INSERT INTO `checkin` VALUES (5, '5503', '2021-07-05');
INSERT INTO `checkin` VALUES (6, '5504', '2021-07-05');
INSERT INTO `checkin` VALUES (7, '5501', '2021-06-28');
INSERT INTO `checkin` VALUES (8, '5502', '2021-06-28');
INSERT INTO `checkin` VALUES (9, '5501', '2021-06-27');
INSERT INTO `checkin` VALUES (10, '5502', '2021-06-26');

-- ----------------------------
-- Table structure for classes
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL COMMENT '班级号',
  `classname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of classes
-- ----------------------------
INSERT INTO `classes` VALUES (1, 55, '涛飞55期');
INSERT INTO `classes` VALUES (2, 56, '涛飞56期');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科目号',
  `cname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科目名称',
  `tno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '老师id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cno`(`cno`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES (1, 'c01', '数学', 't01');
INSERT INTO `course` VALUES (2, 'c02', '语文', 't02');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_name` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cust_address` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_city` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_state` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_zip` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_country` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_contact` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cust_email` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cust_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10006 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (10001, 'Coyote Inc.', '200 Maple Lane', 'Detroit', 'MI', '44444', 'USA', 'Y Lee', 'ylee@coyote.com');
INSERT INTO `customers` VALUES (10002, 'Mouse House', '333 Fromage Lane', 'Columbus', 'OH', '43333', 'USA', 'Jerry Mouse', NULL);
INSERT INTO `customers` VALUES (10003, 'Wascals', '1 Sunny Place', 'Muncie', 'IN', '42222', 'USA', 'Jim Jones', 'rabbit@wascally.com');
INSERT INTO `customers` VALUES (10004, 'Yosemite Place', '829 Riverside Drive', 'Phoenix', 'AZ', '88888', 'USA', 'Y Sam', 'sam@yosemite.com');
INSERT INTO `customers` VALUES (10005, 'E Fudd', '4545 53rd Street', 'Chicago', 'IL', '54545', 'USA', 'E Fudd', NULL);

-- ----------------------------
-- Table structure for orderitems
-- ----------------------------
DROP TABLE IF EXISTS `orderitems`;
CREATE TABLE `orderitems`  (
  `order_num` int(11) NOT NULL,
  `order_item` int(11) NOT NULL,
  `prod_id` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `item_price` decimal(8, 2) NOT NULL,
  PRIMARY KEY (`order_num`, `order_item`) USING BTREE,
  INDEX `fk_orderitems_products`(`prod_id`) USING BTREE,
  CONSTRAINT `fk_orderitems_orders` FOREIGN KEY (`order_num`) REFERENCES `orders` (`order_num`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_orderitems_products` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orderitems
-- ----------------------------
INSERT INTO `orderitems` VALUES (20005, 1, 'ANV01', 10, 5.99);
INSERT INTO `orderitems` VALUES (20005, 2, 'ANV02', 3, 9.99);
INSERT INTO `orderitems` VALUES (20005, 3, 'TNT2', 5, 10.00);
INSERT INTO `orderitems` VALUES (20005, 4, 'FB', 1, 10.00);
INSERT INTO `orderitems` VALUES (20006, 1, 'JP2000', 1, 55.00);
INSERT INTO `orderitems` VALUES (20007, 1, 'TNT2', 100, 10.00);
INSERT INTO `orderitems` VALUES (20008, 1, 'FC', 50, 2.50);
INSERT INTO `orderitems` VALUES (20009, 1, 'FB', 1, 10.00);
INSERT INTO `orderitems` VALUES (20009, 2, 'OL1', 1, 8.99);
INSERT INTO `orderitems` VALUES (20009, 3, 'SLING', 1, 4.49);
INSERT INTO `orderitems` VALUES (20009, 4, 'ANV03', 1, 14.99);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `order_num` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime(0) NOT NULL,
  `cust_id` int(11) NOT NULL,
  PRIMARY KEY (`order_num`) USING BTREE,
  INDEX `fk_orders_customers`(`cust_id`) USING BTREE,
  CONSTRAINT `fk_orders_customers` FOREIGN KEY (`cust_id`) REFERENCES `customers` (`cust_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20010 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (20005, '2020-09-01 00:00:00', 10001);
INSERT INTO `orders` VALUES (20006, '2020-09-12 00:00:00', 10003);
INSERT INTO `orders` VALUES (20007, '2020-09-30 00:00:00', 10004);
INSERT INTO `orders` VALUES (20008, '2020-10-03 00:00:00', 10005);
INSERT INTO `orders` VALUES (20009, '2020-10-08 00:00:00', 10001);

-- ----------------------------
-- Table structure for productnotes
-- ----------------------------
DROP TABLE IF EXISTS `productnotes`;
CREATE TABLE `productnotes`  (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `note_date` datetime(0) NOT NULL,
  `note_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`note_id`) USING BTREE,
  FULLTEXT INDEX `note_text`(`note_text`)
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productnotes
-- ----------------------------
INSERT INTO `productnotes` VALUES (101, 'TNT2', '2020-08-17 00:00:00', 'Customer complaint:\r\nSticks not individually wrapped, too easy to mistakenly detonate all at once.\r\nRecommend individual wrapping.');
INSERT INTO `productnotes` VALUES (102, 'OL1', '2020-08-18 00:00:00', 'Can shipped full, refills not available.\r\nNeed to order new can if refill needed.');
INSERT INTO `productnotes` VALUES (103, 'SAFE', '2020-08-18 00:00:00', 'Safe is combination locked, combination not provided with safe.\r\nThis is rarely a problem as safes are typically blown up or dropped by customers.');
INSERT INTO `productnotes` VALUES (104, 'FC', '2020-08-19 00:00:00', 'Quantity varies, sold by the sack load.\r\nAll guaranteed to be bright and orange, and suitable for use as rabbit bait.');
INSERT INTO `productnotes` VALUES (105, 'TNT2', '2020-08-20 00:00:00', 'Included fuses are short and have been known to detonate too quickly for some customers.\r\nLonger fuses are available (item FU1) and should be recommended.');
INSERT INTO `productnotes` VALUES (106, 'TNT2', '2020-08-22 00:00:00', 'Matches not included, recommend purchase of matches or detonator (item DTNTR).');
INSERT INTO `productnotes` VALUES (107, 'SAFE', '2020-08-23 00:00:00', 'Please note that no returns will be accepted if safe opened using explosives.');
INSERT INTO `productnotes` VALUES (108, 'ANV01', '2020-08-25 00:00:00', 'Multiple customer returns, anvils failing to drop fast enough or falling backwards on purchaser. Recommend that customer considers using heavier anvils.');
INSERT INTO `productnotes` VALUES (109, 'ANV03', '2020-09-01 00:00:00', 'Item is extremely heavy. Designed for dropping, not recommended for use with slings, ropes, pulleys, or tightropes.');
INSERT INTO `productnotes` VALUES (110, 'FC', '2020-09-01 00:00:00', 'Customer complaint: rabbit has been able to detect trap, food apparently less effective now.');
INSERT INTO `productnotes` VALUES (111, 'SLING', '2020-09-02 00:00:00', 'Shipped unassembled, requires common tools (including oversized hammer).');
INSERT INTO `productnotes` VALUES (112, 'SAFE', '2020-09-02 00:00:00', 'Customer complaint:\r\nCircular hole in safe floor can apparently be easily cut with handsaw.');
INSERT INTO `productnotes` VALUES (113, 'ANV01', '2020-09-05 00:00:00', 'Customer complaint:\r\nNot heavy enough to generate flying stars around head of victim. If being purchased for dropping, recommend ANV02 or ANV03 instead.');
INSERT INTO `productnotes` VALUES (114, 'SAFE', '2020-09-07 00:00:00', 'Call from individual trapped in safe plummeting to the ground, suggests an escape hatch be added.\r\nComment forwarded to vendor.');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `prod_id` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vend_id` int(11) NOT NULL,
  `prod_name` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `prod_price` decimal(8, 2) NOT NULL,
  `prod_desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`prod_id`) USING BTREE,
  INDEX `fk_products_vendors`(`vend_id`) USING BTREE,
  CONSTRAINT `fk_products_vendors` FOREIGN KEY (`vend_id`) REFERENCES `vendors` (`vend_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('ANV01', 1001, '.5 ton anvil', 5.99, '.5 ton anvil, black, complete with handy hook');
INSERT INTO `products` VALUES ('ANV02', 1001, '1 ton anvil', 9.99, '1 ton anvil, black, complete with handy hook and carrying case');
INSERT INTO `products` VALUES ('ANV03', 1001, '2 ton anvil', 14.99, '2 ton anvil, black, complete with handy hook and carrying case');
INSERT INTO `products` VALUES ('DTNTR', 1003, 'Detonator', 13.00, 'Detonator (plunger powered), fuses not included');
INSERT INTO `products` VALUES ('FB', 1003, 'Bird seed', 10.00, 'Large bag (suitable for road runners)');
INSERT INTO `products` VALUES ('FC', 1003, 'Carrots', 2.50, 'Carrots (rabbit hunting season only)');
INSERT INTO `products` VALUES ('FU1', 1002, 'Fuses', 3.42, '1 dozen, extra long');
INSERT INTO `products` VALUES ('JP1000', 1005, 'JetPack 1000', 35.00, 'JetPack 1000, intended for single use');
INSERT INTO `products` VALUES ('JP2000', 1005, 'JetPack 2000', 55.00, 'JetPack 2000, multi-use');
INSERT INTO `products` VALUES ('OL1', 1002, 'Oil can', 8.99, 'Oil can, red');
INSERT INTO `products` VALUES ('SAFE', 1003, 'Safe', 50.00, 'Safe with combination lock');
INSERT INTO `products` VALUES ('SLING', 1003, 'Sling', 4.49, 'Sling, one size fits all');
INSERT INTO `products` VALUES ('TNT1', 1003, 'TNT (1 stick)', 2.50, 'TNT, red, single stick');
INSERT INTO `products` VALUES ('TNT2', 1003, 'TNT (5 sticks)', 10.00, 'TNT, red, pack of 10 sticks');

-- ----------------------------
-- Table structure for scores
-- ----------------------------
DROP TABLE IF EXISTS `scores`;
CREATE TABLE `scores`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号',
  `cno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '科目',
  `score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成绩',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `studentno`(`sno`) USING BTREE,
  INDEX `courseno`(`cno`) USING BTREE,
  CONSTRAINT `courseno` FOREIGN KEY (`cno`) REFERENCES `course` (`cno`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `studentno` FOREIGN KEY (`sno`) REFERENCES `student` (`sno`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of scores
-- ----------------------------
INSERT INTO `scores` VALUES (1, '5501', 'c01', '90');
INSERT INTO `scores` VALUES (2, '5502', 'c02', '80');
INSERT INTO `scores` VALUES (3, '5501', 'c02', '70');
INSERT INTO `scores` VALUES (4, '5504', 'c01', '90');
INSERT INTO `scores` VALUES (5, '5507', 'c02', '40');
INSERT INTO `scores` VALUES (6, '5510', 'c02', '60');
INSERT INTO `scores` VALUES (7, '5503', 'c01', '20');
INSERT INTO `scores` VALUES (8, '5598', 'c02', '30');
INSERT INTO `scores` VALUES (9, '5558', 'c02', '60');
INSERT INTO `scores` VALUES (10, '5544', 'c02', '50');
INSERT INTO `scores` VALUES (11, '5586', 'c02', '90');
INSERT INTO `scores` VALUES (12, '5558', 'c01', '80');
INSERT INTO `scores` VALUES (13, '5616', 'c01', '67');
INSERT INTO `scores` VALUES (14, '5618', 'c02', '90');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sno` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学号',
  `sname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生姓名',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别',
  `birthday` date NOT NULL COMMENT '生日',
  `classid` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班级id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sno`(`sno`) USING BTREE,
  INDEX `sname_idx`(`sname`) USING BTREE,
  INDEX `snameidx`(`sname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 459106 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (6, '5501', '罗雷', '男', '1974-12-02', '55');
INSERT INTO `student` VALUES (7, '5502', '詹秀云', '女', '2006-01-29', '55');
INSERT INTO `student` VALUES (8, '5503', '黄柳', '女', '2011-03-05', '55');
INSERT INTO `student` VALUES (9, '5504', '曾莹', '女', '2003-06-24', '55');
INSERT INTO `student` VALUES (10, '5505', '张坤', '男', '1998-05-02', '55');
INSERT INTO `student` VALUES (11, '5506', '陈秀荣', '女', '1977-08-25', '55');
INSERT INTO `student` VALUES (12, '5507', '陈俊', '男', '1986-06-15', '55');
INSERT INTO `student` VALUES (13, '5508', '田明', '女', '1992-11-24', '55');
INSERT INTO `student` VALUES (14, '5509', '吴秀英', '女', '2010-10-19', '55');
INSERT INTO `student` VALUES (15, '5510', '梁娜', '男', '1978-12-30', '55');
INSERT INTO `student` VALUES (16, '5511', '王亮', '女', '1996-02-22', '55');
INSERT INTO `student` VALUES (17, '5512', '何桂英', '男', '2021-05-25', '55');
INSERT INTO `student` VALUES (18, '5513', '文斌', '男', '2014-12-13', '55');
INSERT INTO `student` VALUES (19, '5514', '孙欢', '男', '1974-05-31', '55');
INSERT INTO `student` VALUES (20, '5515', '宫帆', '女', '1985-03-08', '55');
INSERT INTO `student` VALUES (21, '5516', '李刚', '男', '2015-05-22', '55');
INSERT INTO `student` VALUES (22, '5517', '汪艳', '女', '2007-06-09', '55');
INSERT INTO `student` VALUES (23, '5518', '张红霞', '男', '2014-09-19', '55');
INSERT INTO `student` VALUES (24, '5519', '张玉梅', '男', '2014-08-27', '55');
INSERT INTO `student` VALUES (25, '5520', '邓伟', '男', '1994-04-24', '55');
INSERT INTO `student` VALUES (26, '5521', '李兵', '男', '1999-03-24', '55');
INSERT INTO `student` VALUES (27, '5522', '韩洁', '女', '1981-11-18', '55');
INSERT INTO `student` VALUES (28, '5523', '庄英', '男', '2017-12-02', '55');
INSERT INTO `student` VALUES (29, '5524', '史雷', '女', '1975-10-12', '55');
INSERT INTO `student` VALUES (30, '5525', '刘晨', '女', '2017-05-23', '55');
INSERT INTO `student` VALUES (31, '5526', '刘成', '女', '2013-01-20', '55');
INSERT INTO `student` VALUES (32, '5527', '陈红', '男', '2005-01-01', '55');
INSERT INTO `student` VALUES (33, '5528', '王雪梅', '女', '2014-11-18', '55');
INSERT INTO `student` VALUES (34, '5529', '李雪', '男', '1974-01-13', '55');
INSERT INTO `student` VALUES (35, '5530', '王浩', '女', '2001-05-10', '55');
INSERT INTO `student` VALUES (36, '5531', '陶海燕', '女', '1978-09-12', '55');
INSERT INTO `student` VALUES (37, '5532', '包凯', '女', '1978-04-04', '55');
INSERT INTO `student` VALUES (38, '5533', '马峰', '女', '2004-12-01', '55');
INSERT INTO `student` VALUES (39, '5534', '闵欢', '女', '1977-02-07', '55');
INSERT INTO `student` VALUES (40, '5535', '江涛', '女', '1975-05-15', '55');
INSERT INTO `student` VALUES (41, '5536', '谢婷', '男', '2018-03-31', '55');
INSERT INTO `student` VALUES (42, '5537', '李兵', '男', '1970-05-10', '55');
INSERT INTO `student` VALUES (43, '5538', '杨小红', '女', '1973-02-22', '55');
INSERT INTO `student` VALUES (44, '5539', '徐浩', '女', '2008-12-05', '55');
INSERT INTO `student` VALUES (45, '5540', '刘杨', '女', '2007-02-08', '55');
INSERT INTO `student` VALUES (46, '5541', '易想', '女', '2020-07-12', '55');
INSERT INTO `student` VALUES (47, '5542', '王娜', '女', '2005-08-31', '55');
INSERT INTO `student` VALUES (48, '5543', '高红霞', '男', '1995-04-19', '55');
INSERT INTO `student` VALUES (49, '5544', '李岩', '男', '2005-10-31', '55');
INSERT INTO `student` VALUES (50, '5545', '孙玉兰', '男', '2012-07-30', '55');
INSERT INTO `student` VALUES (51, '5546', '白建平', '女', '1998-06-06', '55');
INSERT INTO `student` VALUES (52, '5547', '张斌', '男', '1998-03-10', '55');
INSERT INTO `student` VALUES (53, '5548', '黄柳', '女', '1995-09-09', '55');
INSERT INTO `student` VALUES (54, '5549', '张秀荣', '女', '2016-11-02', '55');
INSERT INTO `student` VALUES (55, '5550', '王帆', '女', '2009-02-27', '55');
INSERT INTO `student` VALUES (56, '5551', '敖建军', '男', '1997-08-13', '55');
INSERT INTO `student` VALUES (57, '5552', '何玉梅', '女', '1982-01-20', '55');
INSERT INTO `student` VALUES (58, '5553', '张志强', '男', '2006-03-08', '55');
INSERT INTO `student` VALUES (59, '5554', '张秀华', '男', '2008-01-07', '55');
INSERT INTO `student` VALUES (60, '5555', '刘娜', '男', '2001-05-23', '55');
INSERT INTO `student` VALUES (61, '5556', '李娟', '女', '2012-07-08', '55');
INSERT INTO `student` VALUES (62, '5557', '魏芳', '男', '2021-05-09', '55');
INSERT INTO `student` VALUES (63, '5558', '张鑫', '女', '1980-09-22', '55');
INSERT INTO `student` VALUES (64, '5559', '陈红', '男', '2011-02-22', '55');
INSERT INTO `student` VALUES (65, '5560', '袁桂兰', '女', '2002-09-10', '55');
INSERT INTO `student` VALUES (66, '5561', '李秀云', '男', '2001-01-05', '55');
INSERT INTO `student` VALUES (67, '5562', '杨凤英', '女', '1988-01-03', '55');
INSERT INTO `student` VALUES (68, '5563', '李建国', '女', '2017-10-18', '55');
INSERT INTO `student` VALUES (69, '5564', '戴颖', '男', '2004-08-21', '55');
INSERT INTO `student` VALUES (70, '5565', '吴倩', '女', '1977-01-17', '55');
INSERT INTO `student` VALUES (71, '5566', '李琴', '男', '2004-10-04', '55');
INSERT INTO `student` VALUES (72, '5567', '陈桂芳', '女', '2001-07-18', '55');
INSERT INTO `student` VALUES (73, '5568', '陈波', '女', '1978-01-26', '55');
INSERT INTO `student` VALUES (74, '5569', '曾秀芳', '女', '2008-11-21', '55');
INSERT INTO `student` VALUES (75, '5570', '张丽娟', '女', '1972-02-04', '55');
INSERT INTO `student` VALUES (76, '5571', '张兰英', '女', '1977-07-13', '55');
INSERT INTO `student` VALUES (77, '5572', '高静', '男', '1991-10-24', '55');
INSERT INTO `student` VALUES (78, '5573', '张博', '女', '1973-05-05', '55');
INSERT INTO `student` VALUES (79, '5574', '魏龙', '女', '2013-08-21', '55');
INSERT INTO `student` VALUES (80, '5575', '张洋', '女', '2011-01-01', '55');
INSERT INTO `student` VALUES (81, '5576', '郭静', '男', '1984-03-03', '55');
INSERT INTO `student` VALUES (82, '5577', '孔阳', '女', '1998-05-27', '55');
INSERT INTO `student` VALUES (83, '5578', '王淑珍', '男', '2006-09-16', '55');
INSERT INTO `student` VALUES (84, '5579', '卢文', '女', '1979-12-23', '55');
INSERT INTO `student` VALUES (85, '5580', '向辉', '男', '2001-08-06', '55');
INSERT INTO `student` VALUES (86, '5581', '刘亮', '男', '2015-09-14', '55');
INSERT INTO `student` VALUES (87, '5582', '张欢', '女', '2006-11-14', '55');
INSERT INTO `student` VALUES (88, '5583', '施燕', '女', '1978-01-06', '55');
INSERT INTO `student` VALUES (89, '5584', '丁冬梅', '女', '1970-06-23', '55');
INSERT INTO `student` VALUES (90, '5585', '林雷', '男', '1984-09-29', '55');
INSERT INTO `student` VALUES (91, '5586', '陈波', '男', '2004-06-15', '55');
INSERT INTO `student` VALUES (92, '5587', '陈瑞', '女', '2007-03-09', '55');
INSERT INTO `student` VALUES (93, '5588', '王秀云', '女', '1999-06-13', '55');
INSERT INTO `student` VALUES (94, '5589', '李梅', '女', '2008-09-20', '55');
INSERT INTO `student` VALUES (95, '5590', '高强', '男', '1974-12-08', '55');
INSERT INTO `student` VALUES (96, '5591', '孙瑞', '女', '1980-11-05', '55');
INSERT INTO `student` VALUES (97, '5592', '张磊', '男', '1999-09-28', '55');
INSERT INTO `student` VALUES (98, '5593', '冯杰', '男', '2017-06-20', '55');
INSERT INTO `student` VALUES (99, '5594', '林军', '女', '1978-05-21', '55');
INSERT INTO `student` VALUES (100, '5595', '冯梅', '女', '1992-01-07', '55');
INSERT INTO `student` VALUES (101, '5596', '廖斌', '女', '2014-08-23', '55');
INSERT INTO `student` VALUES (102, '5597', '黄丽华', '女', '2018-03-21', '55');
INSERT INTO `student` VALUES (103, '5598', '马秀梅', '女', '2011-03-12', '55');
INSERT INTO `student` VALUES (104, '5599', '张欣', '男', '2014-05-22', '55');
INSERT INTO `student` VALUES (105, '5601', '刘宇', '男', '1996-07-18', '56');
INSERT INTO `student` VALUES (106, '5602', '贺慧', '男', '1977-04-23', '56');
INSERT INTO `student` VALUES (107, '5603', '晋燕', '男', '1997-10-02', '56');
INSERT INTO `student` VALUES (108, '5604', '杜建国', '女', '1991-08-09', '56');
INSERT INTO `student` VALUES (109, '5605', '刘洋', '男', '1982-07-27', '56');
INSERT INTO `student` VALUES (110, '5606', '赵刚', '男', '1983-06-21', '56');
INSERT INTO `student` VALUES (111, '5607', '黄林', '男', '1995-10-05', '56');
INSERT INTO `student` VALUES (112, '5608', '湛淑珍', '女', '2014-11-26', '56');
INSERT INTO `student` VALUES (113, '5609', '高淑兰', '男', '1991-03-29', '56');
INSERT INTO `student` VALUES (114, '5610', '王凯', '女', '1990-01-31', '56');
INSERT INTO `student` VALUES (115, '5611', '李涛', '男', '1988-02-16', '56');
INSERT INTO `student` VALUES (116, '5612', '汪鹏', '女', '2018-12-06', '56');
INSERT INTO `student` VALUES (117, '5613', '李雪梅', '女', '2015-04-19', '56');
INSERT INTO `student` VALUES (118, '5614', '周瑞', '女', '1973-09-12', '56');
INSERT INTO `student` VALUES (119, '5615', '倪明', '女', '2001-03-07', '56');
INSERT INTO `student` VALUES (120, '5616', '米梅', '女', '2009-08-27', '56');
INSERT INTO `student` VALUES (121, '5617', '张雷', '女', '2005-09-25', '56');
INSERT INTO `student` VALUES (122, '5618', '易丽丽', '男', '2005-03-27', '56');
INSERT INTO `student` VALUES (123, '5619', '张桂芝', '女', '1985-04-01', '56');
INSERT INTO `student` VALUES (124, '5601', '柴浩', '男', '2018-10-10', '57');
INSERT INTO `student` VALUES (125, '5602', '苏春梅', '女', '2013-07-12', '57');
INSERT INTO `student` VALUES (126, '5603', '王波', '女', '1986-12-04', '57');
INSERT INTO `student` VALUES (127, '5604', '侯健', '男', '1990-03-18', '57');
INSERT INTO `student` VALUES (128, '5605', '范林', '女', '1990-05-15', '57');
INSERT INTO `student` VALUES (129, '5606', '张凯', '女', '1988-10-14', '57');
INSERT INTO `student` VALUES (130, '5607', '李桂珍', '男', '1974-08-05', '57');
INSERT INTO `student` VALUES (131, '5608', '张冬梅', '女', '2010-06-30', '57');
INSERT INTO `student` VALUES (132, '5609', '王龙', '男', '1987-01-02', '57');
INSERT INTO `student` VALUES (133, '5610', '曹桂芳', '男', '1997-12-25', '57');
INSERT INTO `student` VALUES (134, '5611', '唐志强', '男', '1971-12-08', '57');
INSERT INTO `student` VALUES (135, '5612', '王晨', '男', '1986-01-15', '57');
INSERT INTO `student` VALUES (136, '5613', '贾健', '男', '1992-05-09', '57');
INSERT INTO `student` VALUES (137, '5614', '陈雪梅', '女', '2009-03-29', '57');
INSERT INTO `student` VALUES (138, '5615', '李淑兰', '男', '1974-05-18', '57');
INSERT INTO `student` VALUES (139, '5616', '薛明', '男', '2015-02-26', '57');
INSERT INTO `student` VALUES (140, '5617', '杨璐', '男', '1988-12-02', '57');
INSERT INTO `student` VALUES (141, '5618', '张阳', '男', '1989-09-26', '57');
INSERT INTO `student` VALUES (142, '5619', '郑红', '男', '1993-11-12', '57');
INSERT INTO `student` VALUES (143, '5620', '袁静', '男', '2000-09-07', '57');
INSERT INTO `student` VALUES (144, '5621', '赵宇', '男', '2021-01-05', '57');
INSERT INTO `student` VALUES (145, '5622', '徐梅', '女', '1977-04-02', '57');
INSERT INTO `student` VALUES (146, '5623', '黄娟', '女', '1975-01-18', '57');
INSERT INTO `student` VALUES (147, '5624', '范秀云', '女', '2001-10-09', '57');
INSERT INTO `student` VALUES (148, '5625', '周玉', '男', '2000-12-10', '57');
INSERT INTO `student` VALUES (149, '5626', '邱红梅', '女', '1982-10-09', '57');
INSERT INTO `student` VALUES (150, '5627', '倪勇', '女', '1990-02-27', '57');
INSERT INTO `student` VALUES (151, '5628', '周云', '男', '2000-06-26', '57');
INSERT INTO `student` VALUES (152, '5629', '胡宇', '女', '1970-04-20', '57');
INSERT INTO `student` VALUES (153, '5630', '郭文', '女', '2014-07-28', '57');
INSERT INTO `student` VALUES (154, '5631', '周秀云', '女', '2006-02-28', '57');
INSERT INTO `student` VALUES (155, '5632', '舒伟', '女', '1996-07-15', '57');
INSERT INTO `student` VALUES (156, '5633', '徐海燕', '女', '2003-02-25', '57');
INSERT INTO `student` VALUES (157, '5634', '曾秀兰', '男', '1994-10-27', '57');
INSERT INTO `student` VALUES (158, '5635', '洪玉梅', '男', '2019-12-03', '57');
INSERT INTO `student` VALUES (159, '5636', '周玉华', '男', '1982-02-26', '57');
INSERT INTO `student` VALUES (160, '5637', '方红梅', '女', '1987-08-25', '57');
INSERT INTO `student` VALUES (161, '5638', '雷桂香', '男', '2001-04-12', '57');
INSERT INTO `student` VALUES (162, '5639', '王婷婷', '女', '1984-10-04', '57');
INSERT INTO `student` VALUES (163, '5640', '黄磊', '女', '2010-03-26', '57');
INSERT INTO `student` VALUES (164, '5641', '王颖', '男', '2016-03-06', '57');
INSERT INTO `student` VALUES (165, '5642', '刘东', '男', '2006-03-12', '57');
INSERT INTO `student` VALUES (166, '5643', '卢英', '女', '1979-04-23', '57');
INSERT INTO `student` VALUES (167, '5644', '谢琳', '女', '2018-07-30', '57');
INSERT INTO `student` VALUES (168, '5645', '樊婷婷', '女', '1993-01-05', '57');
INSERT INTO `student` VALUES (169, '5646', '杨丽丽', '女', '1979-05-23', '57');
INSERT INTO `student` VALUES (170, '5647', '聂颖', '女', '1991-01-03', '57');
INSERT INTO `student` VALUES (171, '5648', '宋玉珍', '男', '1988-03-30', '57');
INSERT INTO `student` VALUES (172, '5649', '李成', '男', '1974-06-08', '57');
INSERT INTO `student` VALUES (173, '5650', '吕琳', '女', '1989-09-25', '57');
INSERT INTO `student` VALUES (174, '5651', '刘欢', '女', '1978-12-18', '57');
INSERT INTO `student` VALUES (175, '5652', '张秀兰', '女', '1977-03-14', '57');
INSERT INTO `student` VALUES (176, '5653', '李玉珍', '女', '1995-06-07', '57');
INSERT INTO `student` VALUES (177, '5654', '谢玉珍', '男', '1980-09-01', '57');
INSERT INTO `student` VALUES (178, '5655', '刘坤', '男', '2001-12-01', '57');
INSERT INTO `student` VALUES (179, '5656', '张海燕', '男', '2002-01-09', '57');
INSERT INTO `student` VALUES (180, '5657', '刘强', '男', '1980-01-29', '57');
INSERT INTO `student` VALUES (181, '5658', '文莹', '男', '1983-07-06', '57');
INSERT INTO `student` VALUES (182, '5659', '张燕', '男', '2011-05-21', '57');
INSERT INTO `student` VALUES (183, '5660', '梁想', '女', '1995-02-17', '57');
INSERT INTO `student` VALUES (184, '5661', '孙雪', '女', '2016-08-08', '57');
INSERT INTO `student` VALUES (185, '5662', '吴健', '男', '1977-09-10', '57');
INSERT INTO `student` VALUES (186, '5663', '吴兵', '男', '1986-11-20', '57');
INSERT INTO `student` VALUES (187, '5664', '颜秀荣', '女', '2014-02-02', '57');
INSERT INTO `student` VALUES (188, '5665', '刘涛', '男', '1986-12-06', '57');
INSERT INTO `student` VALUES (189, '5666', '赖玉珍', '男', '2020-09-10', '57');
INSERT INTO `student` VALUES (190, '5667', '王小红', '女', '2004-05-19', '57');
INSERT INTO `student` VALUES (191, '5668', '黎丹', '女', '2015-06-30', '57');
INSERT INTO `student` VALUES (192, '5669', '韩杰', '女', '2000-06-06', '57');
INSERT INTO `student` VALUES (193, '5670', '刘桂兰', '女', '1990-08-13', '57');
INSERT INTO `student` VALUES (194, '5671', '郭磊', '男', '1996-12-05', '57');
INSERT INTO `student` VALUES (195, '5672', '袁岩', '女', '1991-03-29', '57');
INSERT INTO `student` VALUES (196, '5673', '鞠勇', '女', '2019-10-17', '57');
INSERT INTO `student` VALUES (197, '5674', '李秀华', '男', '2014-05-02', '57');
INSERT INTO `student` VALUES (198, '5675', '张龙', '女', '2004-08-06', '57');
INSERT INTO `student` VALUES (199, '5676', '许丽', '女', '2004-12-15', '57');
INSERT INTO `student` VALUES (200, '5677', '宋宁', '男', '2015-02-18', '57');
INSERT INTO `student` VALUES (201, '5678', '陈强', '女', '1980-02-18', '57');
INSERT INTO `student` VALUES (202, '5679', '成秀兰', '男', '1972-03-18', '57');
INSERT INTO `student` VALUES (203, '5680', '崔彬', '女', '2017-06-19', '57');
INSERT INTO `student` VALUES (204, '5681', '段建军', '男', '1983-09-13', '57');
INSERT INTO `student` VALUES (205, '5682', '陈玉珍', '男', '1973-06-30', '57');
INSERT INTO `student` VALUES (206, '5683', '王红', '男', '1980-10-24', '57');
INSERT INTO `student` VALUES (207, '5684', '姚宁', '男', '2009-02-16', '57');
INSERT INTO `student` VALUES (208, '5685', '黄萍', '女', '2016-11-06', '57');
INSERT INTO `student` VALUES (209, '5686', '卢丽丽', '女', '1977-11-13', '57');
INSERT INTO `student` VALUES (210, '5687', '刘萍', '男', '2011-06-08', '57');
INSERT INTO `student` VALUES (211, '5688', '刘岩', '男', '1977-11-02', '57');
INSERT INTO `student` VALUES (212, '5689', '杨建华', '男', '1990-04-25', '57');
INSERT INTO `student` VALUES (213, '5690', '徐坤', '男', '2012-11-16', '57');
INSERT INTO `student` VALUES (214, '5691', '陈琴', '女', '1981-10-31', '57');
INSERT INTO `student` VALUES (215, '5692', '赵桂英', '男', '2004-05-20', '57');
INSERT INTO `student` VALUES (216, '5693', '张敏', '男', '1975-05-29', '57');
INSERT INTO `student` VALUES (217, '5694', '江倩', '女', '1971-12-16', '57');
INSERT INTO `student` VALUES (218, '5695', '张波', '男', '1996-04-05', '57');
INSERT INTO `student` VALUES (219, '5696', '谢颖', '女', '1984-09-30', '57');
INSERT INTO `student` VALUES (220, '5697', '何红霞', '女', '1985-05-16', '57');
INSERT INTO `student` VALUES (221, '5698', '蒋雪', '女', '1990-02-02', '57');
INSERT INTO `student` VALUES (222, '5699', '李丽华', '男', '1985-12-12', '57');
INSERT INTO `student` VALUES (223, '56100', '丁玉英', '女', '2015-08-06', '57');
INSERT INTO `student` VALUES (224, '56101', '刘平', '女', '1970-07-16', '57');
INSERT INTO `student` VALUES (225, '56102', '沈丹丹', '女', '2017-01-07', '57');
INSERT INTO `student` VALUES (226, '56103', '姚明', '女', '2003-04-22', '57');
INSERT INTO `student` VALUES (227, '56104', '赵建军', '男', '2010-04-21', '57');
INSERT INTO `student` VALUES (228, '56105', '张桂花', '女', '1984-11-06', '57');
INSERT INTO `student` VALUES (229, '56106', '游丽', '男', '1980-05-26', '57');
INSERT INTO `student` VALUES (230, '56107', '黄丹', '男', '2019-12-02', '57');
INSERT INTO `student` VALUES (231, '56108', '高春梅', '女', '1978-01-05', '57');
INSERT INTO `student` VALUES (232, '56109', '吕利', '男', '1973-12-24', '57');
INSERT INTO `student` VALUES (233, '56110', '杨琳', '男', '2009-01-13', '57');
INSERT INTO `student` VALUES (234, '56111', '信娜', '女', '1973-03-31', '57');
INSERT INTO `student` VALUES (235, '56112', '黄涛', '女', '2020-11-07', '57');
INSERT INTO `student` VALUES (236, '56113', '王丽娟', '男', '1991-12-14', '57');
INSERT INTO `student` VALUES (237, '56114', '蓝鹏', '男', '2013-06-06', '57');
INSERT INTO `student` VALUES (238, '56115', '胡秀梅', '女', '2009-04-08', '57');
INSERT INTO `student` VALUES (239, '56116', '孟建华', '男', '1989-08-20', '57');
INSERT INTO `student` VALUES (240, '56117', '贺凤英', '男', '2021-03-13', '57');
INSERT INTO `student` VALUES (241, '56118', '王凤兰', '女', '2013-05-05', '57');
INSERT INTO `student` VALUES (242, '56119', '王佳', '女', '2019-01-15', '57');
INSERT INTO `student` VALUES (243, '56120', '欧丽娟', '女', '1970-08-12', '57');
INSERT INTO `student` VALUES (244, '56121', '牛海燕', '女', '2021-03-09', '57');
INSERT INTO `student` VALUES (245, '56122', '许磊', '男', '2009-01-17', '57');
INSERT INTO `student` VALUES (246, '56123', '李玉梅', '男', '1993-09-11', '57');
INSERT INTO `student` VALUES (247, '56124', '刘小红', '男', '1986-03-23', '57');
INSERT INTO `student` VALUES (248, '56125', '杨婷', '女', '1991-05-16', '57');
INSERT INTO `student` VALUES (249, '56126', '孙雷', '女', '2004-04-16', '57');
INSERT INTO `student` VALUES (250, '56127', '黄颖', '女', '2002-07-16', '57');
INSERT INTO `student` VALUES (251, '56128', '程坤', '女', '1972-03-03', '57');
INSERT INTO `student` VALUES (252, '56129', '鞠建华', '男', '2017-08-21', '57');
INSERT INTO `student` VALUES (253, '56130', '陈丹', '女', '1990-04-28', '57');
INSERT INTO `student` VALUES (254, '56131', '秦秀芳', '男', '2009-08-26', '57');
INSERT INTO `student` VALUES (255, '56132', '王燕', '女', '1972-09-05', '57');
INSERT INTO `student` VALUES (256, '56133', '宋波', '男', '1980-02-08', '57');
INSERT INTO `student` VALUES (257, '56134', '汤晶', '女', '1989-07-03', '57');
INSERT INTO `student` VALUES (258, '56135', '张金凤', '男', '2005-01-03', '57');
INSERT INTO `student` VALUES (259, '56136', '苏艳', '男', '2011-11-24', '57');
INSERT INTO `student` VALUES (260, '56137', '陈玲', '女', '2019-02-10', '57');
INSERT INTO `student` VALUES (261, '56138', '项慧', '女', '1983-02-11', '57');
INSERT INTO `student` VALUES (262, '56139', '张建平', '男', '1999-07-11', '57');
INSERT INTO `student` VALUES (263, '56140', '孙丽华', '男', '1994-07-31', '57');
INSERT INTO `student` VALUES (264, '56141', '杨柳', '男', '2007-09-17', '57');
INSERT INTO `student` VALUES (265, '56142', '高帆', '女', '2013-09-11', '57');
INSERT INTO `student` VALUES (266, '56143', '赵颖', '男', '2017-07-04', '57');
INSERT INTO `student` VALUES (267, '56144', '张阳', '男', '1996-12-26', '57');
INSERT INTO `student` VALUES (268, '56145', '沈燕', '女', '1995-09-16', '57');
INSERT INTO `student` VALUES (269, '56146', '林萍', '女', '1995-08-04', '57');
INSERT INTO `student` VALUES (270, '56147', '陈柳', '女', '2018-10-08', '57');
INSERT INTO `student` VALUES (271, '56148', '周秀珍', '男', '1988-10-11', '57');
INSERT INTO `student` VALUES (272, '56149', '张畅', '男', '2003-07-08', '57');
INSERT INTO `student` VALUES (273, '56150', '李刚', '女', '2019-10-20', '57');
INSERT INTO `student` VALUES (274, '56151', '陈想', '女', '2002-12-10', '57');
INSERT INTO `student` VALUES (275, '56152', '申健', '女', '1976-06-25', '57');
INSERT INTO `student` VALUES (276, '56153', '李芳', '女', '2011-09-20', '57');
INSERT INTO `student` VALUES (277, '56154', '陈淑珍', '女', '1976-07-25', '57');
INSERT INTO `student` VALUES (278, '56155', '徐艳', '男', '1998-05-07', '57');
INSERT INTO `student` VALUES (279, '56156', '程秀兰', '男', '1997-02-25', '57');
INSERT INTO `student` VALUES (280, '56157', '钱玉华', '男', '1990-02-10', '57');
INSERT INTO `student` VALUES (281, '56158', '王明', '女', '1988-11-19', '57');
INSERT INTO `student` VALUES (282, '56159', '张桂花', '女', '1979-04-21', '57');
INSERT INTO `student` VALUES (283, '56160', '葛畅', '男', '1994-08-22', '57');
INSERT INTO `student` VALUES (284, '56161', '柴欢', '男', '2001-07-09', '57');
INSERT INTO `student` VALUES (285, '56162', '王丹', '女', '1998-09-05', '57');
INSERT INTO `student` VALUES (286, '56163', '董桂荣', '男', '1985-11-15', '57');
INSERT INTO `student` VALUES (287, '56164', '汪东', '男', '1972-08-29', '57');
INSERT INTO `student` VALUES (288, '56165', '陈秀珍', '男', '1975-12-19', '57');
INSERT INTO `student` VALUES (289, '56166', '陈燕', '女', '1985-12-28', '57');
INSERT INTO `student` VALUES (290, '56167', '杨小红', '女', '1987-08-18', '57');
INSERT INTO `student` VALUES (291, '56168', '张帆', '女', '2008-10-15', '57');
INSERT INTO `student` VALUES (292, '56169', '马斌', '男', '1981-12-13', '57');
INSERT INTO `student` VALUES (293, '56170', '柳丽华', '男', '1993-07-11', '57');
INSERT INTO `student` VALUES (294, '56171', '张娜', '女', '2015-05-22', '57');
INSERT INTO `student` VALUES (295, '56172', '林建军', '男', '2019-06-02', '57');
INSERT INTO `student` VALUES (296, '56173', '马秀珍', '女', '1984-06-27', '57');
INSERT INTO `student` VALUES (297, '56174', '周畅', '女', '1976-10-12', '57');
INSERT INTO `student` VALUES (298, '56175', '张金凤', '男', '1973-02-04', '57');
INSERT INTO `student` VALUES (299, '56176', '周春梅', '男', '1972-01-21', '57');
INSERT INTO `student` VALUES (300, '56177', '刘丽丽', '女', '1996-03-29', '57');
INSERT INTO `student` VALUES (301, '56178', '张璐', '女', '2010-07-09', '57');
INSERT INTO `student` VALUES (302, '56179', '郑兵', '男', '1998-04-25', '57');
INSERT INTO `student` VALUES (303, '56180', '夏成', '男', '2016-02-18', '57');
INSERT INTO `student` VALUES (304, '56181', '赵佳', '男', '1983-09-12', '57');
INSERT INTO `student` VALUES (305, '56182', '和磊', '男', '2014-11-30', '57');
INSERT INTO `student` VALUES (306, '56183', '吴燕', '男', '1995-10-03', '57');
INSERT INTO `student` VALUES (307, '56184', '崔秀芳', '女', '1971-11-18', '57');
INSERT INTO `student` VALUES (308, '56185', '徐龙', '女', '1970-09-21', '57');
INSERT INTO `student` VALUES (309, '56186', '王欢', '女', '1998-11-26', '57');
INSERT INTO `student` VALUES (310, '56187', '高雷', '男', '1983-08-27', '57');
INSERT INTO `student` VALUES (311, '56188', '孙淑兰', '女', '1975-06-04', '57');
INSERT INTO `student` VALUES (312, '56189', '黄春梅', '女', '1991-04-22', '57');
INSERT INTO `student` VALUES (313, '56190', '马秀云', '女', '1985-07-01', '57');
INSERT INTO `student` VALUES (314, '56191', '黄峰', '男', '1977-09-10', '57');
INSERT INTO `student` VALUES (315, '56192', '武倩', '男', '1991-06-13', '57');
INSERT INTO `student` VALUES (316, '56193', '王淑华', '男', '2001-08-28', '57');
INSERT INTO `student` VALUES (317, '56194', '田红', '女', '2017-11-09', '57');
INSERT INTO `student` VALUES (318, '56195', '雷玉珍', '女', '1973-07-30', '57');
INSERT INTO `student` VALUES (319, '56196', '陈芳', '女', '2019-12-10', '57');
INSERT INTO `student` VALUES (320, '56197', '吴欢', '男', '2010-03-02', '57');
INSERT INTO `student` VALUES (321, '56198', '刘晶', '男', '1970-06-23', '57');
INSERT INTO `student` VALUES (322, '56199', '杨建平', '男', '1982-09-19', '57');
INSERT INTO `student` VALUES (323, '56200', '王伟', '男', '2016-04-22', '57');
INSERT INTO `student` VALUES (324, '56201', '李利', '女', '1982-01-05', '57');
INSERT INTO `student` VALUES (325, '56202', '杨勇', '男', '2021-04-02', '57');
INSERT INTO `student` VALUES (326, '56203', '王秀荣', '男', '1978-06-14', '57');
INSERT INTO `student` VALUES (327, '56204', '浦旭', '男', '2005-07-05', '57');
INSERT INTO `student` VALUES (328, '56205', '王红霞', '女', '1981-01-03', '57');
INSERT INTO `student` VALUES (329, '56206', '尹桂花', '女', '1999-12-18', '57');
INSERT INTO `student` VALUES (330, '56207', '时秀梅', '男', '1984-03-28', '57');
INSERT INTO `student` VALUES (331, '56208', '唐兰英', '女', '1974-02-15', '57');
INSERT INTO `student` VALUES (332, '56209', '汪磊', '男', '2004-02-10', '57');
INSERT INTO `student` VALUES (333, '56210', '王佳', '男', '2003-11-10', '57');
INSERT INTO `student` VALUES (334, '56211', '江丹丹', '男', '1983-02-23', '57');
INSERT INTO `student` VALUES (335, '56212', '张兰英', '男', '2009-06-25', '57');
INSERT INTO `student` VALUES (336, '56213', '王洋', '女', '2005-11-16', '57');
INSERT INTO `student` VALUES (337, '56214', '杜荣', '女', '1999-11-24', '57');
INSERT INTO `student` VALUES (338, '56215', '贺倩', '女', '1972-03-06', '57');
INSERT INTO `student` VALUES (339, '56216', '罗玉梅', '女', '2016-10-18', '57');
INSERT INTO `student` VALUES (340, '56217', '尉瑞', '女', '1995-06-15', '57');
INSERT INTO `student` VALUES (341, '56218', '刘淑英', '男', '2004-05-26', '57');
INSERT INTO `student` VALUES (342, '56219', '李凤兰', '男', '1973-04-11', '57');
INSERT INTO `student` VALUES (343, '56220', '罗琴', '男', '2005-06-02', '57');
INSERT INTO `student` VALUES (344, '56221', '赵玉珍', '男', '1979-05-12', '57');
INSERT INTO `student` VALUES (345, '56222', '李萍', '男', '1985-03-20', '57');
INSERT INTO `student` VALUES (346, '56223', '覃华', '男', '2019-08-07', '57');
INSERT INTO `student` VALUES (347, '56224', '张建', '女', '1982-12-31', '57');
INSERT INTO `student` VALUES (348, '56225', '姜龙', '女', '2021-06-16', '57');
INSERT INTO `student` VALUES (349, '56226', '姚莹', '女', '1981-02-13', '57');
INSERT INTO `student` VALUES (350, '56227', '张欢', '女', '1977-11-18', '57');
INSERT INTO `student` VALUES (351, '56228', '陈琴', '女', '1991-06-20', '57');
INSERT INTO `student` VALUES (352, '56229', '崔华', '男', '1978-10-31', '57');
INSERT INTO `student` VALUES (353, '56230', '赵丹', '男', '1973-04-06', '57');
INSERT INTO `student` VALUES (354, '56231', '田超', '女', '1992-04-25', '57');
INSERT INTO `student` VALUES (355, '56232', '傅桂香', '男', '2006-04-03', '57');
INSERT INTO `student` VALUES (356, '56233', '刘强', '男', '1973-10-13', '57');
INSERT INTO `student` VALUES (357, '56234', '解浩', '女', '1974-06-26', '57');
INSERT INTO `student` VALUES (358, '56235', '韦凤兰', '男', '2016-02-15', '57');
INSERT INTO `student` VALUES (359, '56236', '林丹丹', '女', '2001-10-21', '57');
INSERT INTO `student` VALUES (360, '56237', '田明', '男', '2000-09-16', '57');
INSERT INTO `student` VALUES (361, '56238', '龙楠', '女', '2019-04-17', '57');
INSERT INTO `student` VALUES (362, '56239', '田楠', '男', '2009-03-31', '57');
INSERT INTO `student` VALUES (363, '56240', '单平', '男', '2019-07-27', '57');
INSERT INTO `student` VALUES (364, '56241', '周丹', '女', '1976-05-17', '57');
INSERT INTO `student` VALUES (365, '56242', '郭冬梅', '女', '2006-05-23', '57');
INSERT INTO `student` VALUES (366, '56243', '余帅', '女', '2009-01-18', '57');
INSERT INTO `student` VALUES (367, '56244', '荣婷', '男', '1971-01-05', '57');
INSERT INTO `student` VALUES (368, '56245', '刘刚', '男', '2004-12-03', '57');
INSERT INTO `student` VALUES (369, '56246', '唐兵', '女', '1994-12-25', '57');
INSERT INTO `student` VALUES (370, '56247', '韩勇', '男', '2003-01-14', '57');
INSERT INTO `student` VALUES (371, '56248', '白慧', '女', '1998-09-19', '57');
INSERT INTO `student` VALUES (372, '56249', '王玉', '男', '1970-10-15', '57');
INSERT INTO `student` VALUES (373, '56250', '龚秀兰', '女', '1994-01-29', '57');
INSERT INTO `student` VALUES (374, '56251', '吴慧', '男', '2019-02-10', '57');
INSERT INTO `student` VALUES (375, '56252', '林桂芝', '男', '2013-12-18', '57');
INSERT INTO `student` VALUES (376, '56253', '董文', '男', '1998-02-26', '57');
INSERT INTO `student` VALUES (377, '56254', '张丹', '女', '2006-07-29', '57');
INSERT INTO `student` VALUES (378, '56255', '徐丽娟', '女', '2016-12-11', '57');
INSERT INTO `student` VALUES (379, '56256', '韩帅', '女', '2019-11-27', '57');
INSERT INTO `student` VALUES (380, '56257', '岳荣', '女', '1976-11-15', '57');
INSERT INTO `student` VALUES (381, '56258', '刘淑英', '男', '2017-05-05', '57');
INSERT INTO `student` VALUES (382, '56259', '王畅', '女', '1983-11-21', '57');
INSERT INTO `student` VALUES (383, '56260', '唐勇', '男', '2007-11-26', '57');
INSERT INTO `student` VALUES (384, '56261', '梁彬', '男', '2014-12-21', '57');
INSERT INTO `student` VALUES (385, '56262', '焦磊', '男', '2005-09-03', '57');
INSERT INTO `student` VALUES (386, '56263', '刘佳', '女', '1986-02-15', '57');
INSERT INTO `student` VALUES (387, '56264', '胡敏', '男', '1985-07-29', '57');
INSERT INTO `student` VALUES (388, '56265', '杜佳', '女', '1996-09-18', '57');
INSERT INTO `student` VALUES (389, '56266', '张博', '女', '1978-04-15', '57');
INSERT INTO `student` VALUES (390, '56267', '李敏', '女', '1973-11-20', '57');
INSERT INTO `student` VALUES (391, '56268', '赵刚', '女', '2021-06-06', '57');
INSERT INTO `student` VALUES (392, '56269', '费志强', '男', '2019-11-04', '57');
INSERT INTO `student` VALUES (393, '56270', '裴冬梅', '男', '1976-03-17', '57');
INSERT INTO `student` VALUES (394, '56271', '朱瑜', '女', '2007-03-05', '57');
INSERT INTO `student` VALUES (395, '56272', '郭勇', '男', '1990-06-23', '57');
INSERT INTO `student` VALUES (396, '56273', '唐玉珍', '男', '2021-02-28', '57');
INSERT INTO `student` VALUES (397, '56274', '张成', '女', '1982-07-20', '57');
INSERT INTO `student` VALUES (398, '56275', '林帅', '女', '2018-03-11', '57');
INSERT INTO `student` VALUES (399, '56276', '陈建国', '女', '2019-12-22', '57');
INSERT INTO `student` VALUES (400, '56277', '俞宇', '女', '2007-10-18', '57');
INSERT INTO `student` VALUES (401, '56278', '刘凤英', '女', '2003-07-29', '57');
INSERT INTO `student` VALUES (402, '56279', '郭俊', '女', '2002-03-15', '57');
INSERT INTO `student` VALUES (403, '56280', '曾杨', '女', '2017-07-25', '57');
INSERT INTO `student` VALUES (404, '56281', '张健', '女', '2000-02-09', '57');
INSERT INTO `student` VALUES (405, '56282', '刘辉', '女', '2017-08-29', '57');
INSERT INTO `student` VALUES (406, '56283', '孙柳', '男', '1996-09-06', '57');
INSERT INTO `student` VALUES (407, '56284', '侯莹', '女', '1990-01-19', '57');
INSERT INTO `student` VALUES (408, '56285', '杜桂珍', '女', '1972-05-16', '57');
INSERT INTO `student` VALUES (409, '56286', '陈琳', '女', '1990-09-08', '57');
INSERT INTO `student` VALUES (410, '56287', '陈伟', '女', '1981-04-23', '57');
INSERT INTO `student` VALUES (411, '56288', '任刚', '女', '2011-03-03', '57');
INSERT INTO `student` VALUES (412, '56289', '高玉兰', '女', '2011-01-20', '57');
INSERT INTO `student` VALUES (413, '56290', '范雷', '男', '2011-11-02', '57');
INSERT INTO `student` VALUES (414, '56291', '陈云', '女', '1982-09-15', '57');
INSERT INTO `student` VALUES (415, '56292', '董玉兰', '男', '1974-06-21', '57');
INSERT INTO `student` VALUES (416, '56293', '桑淑兰', '男', '2002-06-13', '57');
INSERT INTO `student` VALUES (417, '56294', '邓志强', '男', '1993-02-08', '57');
INSERT INTO `student` VALUES (418, '56295', '卫桂荣', '女', '1998-01-07', '57');
INSERT INTO `student` VALUES (419, '56296', '杨龙', '女', '2001-03-31', '57');
INSERT INTO `student` VALUES (420, '56297', '张雷', '男', '1997-12-17', '57');
INSERT INTO `student` VALUES (421, '56298', '阎梅', '女', '1972-03-04', '57');
INSERT INTO `student` VALUES (422, '56299', '杨刚', '女', '1988-11-10', '57');
INSERT INTO `student` VALUES (423, '56300', '李建华', '女', '2003-06-25', '57');
INSERT INTO `student` VALUES (424, '56301', '车凤英', '男', '2002-03-23', '57');
INSERT INTO `student` VALUES (425, '56302', '王艳', '女', '1990-12-26', '57');
INSERT INTO `student` VALUES (426, '56303', '龚海燕', '男', '2015-10-21', '57');
INSERT INTO `student` VALUES (427, '56304', '陈金凤', '男', '1978-10-15', '57');
INSERT INTO `student` VALUES (428, '56305', '周旭', '女', '1993-11-26', '57');
INSERT INTO `student` VALUES (429, '56306', '樊萍', '女', '2000-04-13', '57');
INSERT INTO `student` VALUES (430, '56307', '朱冬梅', '男', '1999-09-10', '57');
INSERT INTO `student` VALUES (431, '56308', '李玉华', '女', '2010-03-27', '57');
INSERT INTO `student` VALUES (432, '56309', '张秀华', '女', '1982-08-01', '57');
INSERT INTO `student` VALUES (433, '56310', '李春梅', '男', '1981-02-10', '57');
INSERT INTO `student` VALUES (434, '56311', '张冬梅', '女', '2012-05-12', '57');
INSERT INTO `student` VALUES (435, '56312', '郑楠', '女', '2001-03-03', '57');
INSERT INTO `student` VALUES (436, '56313', '石琴', '男', '2007-09-07', '57');
INSERT INTO `student` VALUES (437, '56314', '王丽华', '男', '2002-05-25', '57');
INSERT INTO `student` VALUES (438, '56315', '粟志强', '男', '1991-01-19', '57');
INSERT INTO `student` VALUES (439, '56316', '姚静', '女', '1994-07-11', '57');
INSERT INTO `student` VALUES (440, '56317', '朱旭', '女', '1977-05-29', '57');
INSERT INTO `student` VALUES (441, '56318', '王玉华', '女', '1994-01-23', '57');
INSERT INTO `student` VALUES (442, '56319', '吴亮', '男', '2013-03-24', '57');
INSERT INTO `student` VALUES (443, '56320', '田磊', '女', '1997-11-09', '57');
INSERT INTO `student` VALUES (444, '56321', '吕婷婷', '男', '1976-08-14', '57');
INSERT INTO `student` VALUES (445, '56322', '王秀梅', '女', '1999-08-03', '57');
INSERT INTO `student` VALUES (446, '56323', '王阳', '女', '2004-08-20', '57');
INSERT INTO `student` VALUES (447, '56324', '郑玉英', '男', '1972-06-30', '57');
INSERT INTO `student` VALUES (448, '56325', '梁鹏', '男', '1971-11-14', '57');
INSERT INTO `student` VALUES (449, '56326', '齐淑珍', '男', '1979-02-28', '57');
INSERT INTO `student` VALUES (450, '56327', '王建军', '男', '2017-05-07', '57');
INSERT INTO `student` VALUES (451, '56328', '王楠', '女', '2002-10-28', '57');
INSERT INTO `student` VALUES (452, '56329', '戚小红', '男', '1994-11-21', '57');
INSERT INTO `student` VALUES (453, '56330', '曾淑兰', '女', '1996-03-30', '57');
INSERT INTO `student` VALUES (454, '56331', '谭平', '女', '1999-04-26', '57');
INSERT INTO `student` VALUES (455, '56332', '赵洁', '女', '1979-11-28', '57');
INSERT INTO `student` VALUES (456, '56333', '王阳', '女', '1993-01-12', '57');
INSERT INTO `student` VALUES (457, '56334', '徐慧', '男', '2015-12-19', '57');
INSERT INTO `student` VALUES (458, '56335', '张杰', '男', '1975-01-22', '57');
INSERT INTO `student` VALUES (459, '56336', '谷鑫', '女', '2014-03-24', '57');
INSERT INTO `student` VALUES (460, '56337', '谌凤英', '女', '2003-11-03', '57');
INSERT INTO `student` VALUES (461, '56338', '陈英', '女', '1999-11-07', '57');
INSERT INTO `student` VALUES (462, '56339', '刘兵', '女', '1991-10-13', '57');
INSERT INTO `student` VALUES (463, '56340', '赵婷婷', '男', '1997-04-19', '57');
INSERT INTO `student` VALUES (464, '56341', '吴桂香', '男', '2003-11-07', '57');
INSERT INTO `student` VALUES (465, '56342', '方波', '女', '1992-06-10', '57');
INSERT INTO `student` VALUES (466, '56343', '温建国', '男', '1996-06-18', '57');
INSERT INTO `student` VALUES (467, '56344', '赵晶', '男', '1976-08-21', '57');
INSERT INTO `student` VALUES (468, '56345', '吴超', '女', '2001-10-05', '57');
INSERT INTO `student` VALUES (469, '56346', '邓峰', '男', '2020-04-01', '57');
INSERT INTO `student` VALUES (470, '56347', '李秀荣', '男', '1987-01-04', '57');
INSERT INTO `student` VALUES (471, '56348', '刘龙', '男', '2017-03-09', '57');
INSERT INTO `student` VALUES (472, '56349', '张娜', '男', '1975-11-01', '57');
INSERT INTO `student` VALUES (473, '56350', '陈丽娟', '女', '1982-10-27', '57');
INSERT INTO `student` VALUES (474, '56351', '李柳', '女', '2005-07-23', '57');
INSERT INTO `student` VALUES (475, '56352', '潘莉', '女', '2015-10-14', '57');
INSERT INTO `student` VALUES (476, '56353', '刘磊', '男', '2016-09-04', '57');
INSERT INTO `student` VALUES (477, '56354', '赵秀英', '男', '2015-01-23', '57');
INSERT INTO `student` VALUES (478, '56355', '吴娜', '女', '2019-01-27', '57');
INSERT INTO `student` VALUES (479, '56356', '杜洁', '男', '2014-09-28', '57');
INSERT INTO `student` VALUES (480, '56357', '孙莉', '女', '2000-02-25', '57');
INSERT INTO `student` VALUES (481, '56358', '李建军', '女', '1998-03-27', '57');
INSERT INTO `student` VALUES (482, '56359', '王玲', '男', '1980-12-05', '57');
INSERT INTO `student` VALUES (483, '56360', '雷娟', '男', '1988-02-28', '57');
INSERT INTO `student` VALUES (484, '56361', '王健', '男', '1982-09-17', '57');
INSERT INTO `student` VALUES (485, '56362', '刘桂芳', '男', '1988-10-22', '57');
INSERT INTO `student` VALUES (486, '56363', '彭丹丹', '男', '1998-09-16', '57');
INSERT INTO `student` VALUES (487, '56364', '张秀兰', '男', '1987-10-04', '57');
INSERT INTO `student` VALUES (488, '56365', '韦丹丹', '女', '1988-01-10', '57');
INSERT INTO `student` VALUES (489, '56366', '张楠', '女', '1989-07-15', '57');
INSERT INTO `student` VALUES (490, '56367', '任丽娟', '女', '1971-01-07', '57');
INSERT INTO `student` VALUES (491, '56368', '孙晶', '女', '2007-12-17', '57');
INSERT INTO `student` VALUES (492, '56369', '萧桂香', '男', '1980-11-22', '57');
INSERT INTO `student` VALUES (493, '56370', '王浩', '男', '2004-01-22', '57');
INSERT INTO `student` VALUES (494, '56371', '刘超', '女', '1980-08-31', '57');
INSERT INTO `student` VALUES (495, '56372', '何鑫', '女', '1987-05-05', '57');
INSERT INTO `student` VALUES (496, '56373', '金淑兰', '女', '1977-06-21', '57');
INSERT INTO `student` VALUES (497, '56374', '陈秀英', '女', '2004-12-29', '57');
INSERT INTO `student` VALUES (498, '56375', '韩成', '男', '2000-12-30', '57');
INSERT INTO `student` VALUES (499, '56376', '赵秀梅', '男', '1986-01-05', '57');
INSERT INTO `student` VALUES (500, '56377', '靳文', '男', '1997-03-17', '57');
INSERT INTO `student` VALUES (501, '56378', '宁建国', '男', '2019-07-04', '57');
INSERT INTO `student` VALUES (502, '56379', '阎雪梅', '男', '2012-12-26', '57');
INSERT INTO `student` VALUES (503, '56380', '吴玉', '女', '2019-02-09', '57');
INSERT INTO `student` VALUES (504, '56381', '朱平', '女', '2007-04-10', '57');
INSERT INTO `student` VALUES (505, '56382', '夏想', '女', '2019-02-18', '57');
INSERT INTO `student` VALUES (506, '56383', '王秀云', '女', '2009-08-19', '57');
INSERT INTO `student` VALUES (507, '56384', '周秀荣', '女', '1989-05-10', '57');
INSERT INTO `student` VALUES (508, '56385', '张娟', '女', '1995-02-27', '57');
INSERT INTO `student` VALUES (509, '56386', '胡文', '女', '2001-05-18', '57');
INSERT INTO `student` VALUES (510, '56387', '李伟', '女', '2003-10-19', '57');
INSERT INTO `student` VALUES (511, '56388', '王淑华', '女', '1983-10-07', '57');
INSERT INTO `student` VALUES (512, '56389', '李淑英', '女', '1984-07-03', '57');
INSERT INTO `student` VALUES (513, '56390', '郭志强', '男', '1993-04-24', '57');
INSERT INTO `student` VALUES (514, '56391', '张燕', '男', '2017-07-11', '57');
INSERT INTO `student` VALUES (515, '56392', '张建', '女', '1994-08-10', '57');
INSERT INTO `student` VALUES (516, '56393', '黄海燕', '女', '1975-02-20', '57');
INSERT INTO `student` VALUES (517, '56394', '魏玉兰', '女', '2015-05-02', '57');
INSERT INTO `student` VALUES (518, '56395', '张兵', '女', '1993-06-16', '57');
INSERT INTO `student` VALUES (519, '56396', '曹刚', '男', '1978-05-12', '57');
INSERT INTO `student` VALUES (520, '56397', '马利', '男', '1997-05-07', '57');
INSERT INTO `student` VALUES (521, '56398', '赵浩', '男', '2016-04-04', '57');
INSERT INTO `student` VALUES (522, '56399', '杨红梅', '男', '1989-05-13', '57');
INSERT INTO `student` VALUES (523, '56400', '李鹏', '男', '1978-01-27', '57');
INSERT INTO `student` VALUES (524, '56401', '窦燕', '男', '2000-02-21', '57');
INSERT INTO `student` VALUES (525, '56402', '李桂荣', '女', '1980-01-11', '57');
INSERT INTO `student` VALUES (526, '56403', '韩东', '女', '1995-01-11', '57');
INSERT INTO `student` VALUES (527, '56404', '李桂花', '女', '2009-07-09', '57');
INSERT INTO `student` VALUES (528, '56405', '王健', '男', '1998-04-21', '57');
INSERT INTO `student` VALUES (529, '56406', '李雪梅', '女', '2010-07-24', '57');
INSERT INTO `student` VALUES (530, '56407', '李强', '女', '1986-10-04', '57');
INSERT INTO `student` VALUES (531, '56408', '王燕', '男', '1985-11-12', '57');
INSERT INTO `student` VALUES (532, '56409', '陈婷婷', '男', '2001-05-19', '57');
INSERT INTO `student` VALUES (533, '56410', '黄洁', '男', '2019-07-13', '57');
INSERT INTO `student` VALUES (534, '56411', '马佳', '男', '1982-01-12', '57');
INSERT INTO `student` VALUES (535, '56412', '王欢', '女', '2009-11-27', '57');
INSERT INTO `student` VALUES (536, '56413', '柏建平', '女', '2003-09-03', '57');
INSERT INTO `student` VALUES (537, '56414', '赵秀英', '女', '2009-01-30', '57');
INSERT INTO `student` VALUES (538, '56415', '刘柳', '女', '1970-11-10', '57');
INSERT INTO `student` VALUES (539, '56416', '黄艳', '女', '2000-05-22', '57');
INSERT INTO `student` VALUES (540, '56417', '王岩', '男', '1980-12-03', '57');
INSERT INTO `student` VALUES (541, '56418', '李玉英', '女', '2014-11-14', '57');
INSERT INTO `student` VALUES (542, '56419', '潘志强', '女', '2019-08-29', '57');
INSERT INTO `student` VALUES (543, '56420', '侯琴', '男', '2012-06-17', '57');
INSERT INTO `student` VALUES (544, '56421', '杨杨', '男', '1971-11-13', '57');
INSERT INTO `student` VALUES (545, '56422', '封小红', '男', '1978-05-24', '57');
INSERT INTO `student` VALUES (546, '56423', '张雷', '女', '1991-09-13', '57');
INSERT INTO `student` VALUES (547, '56424', '桑秀云', '女', '1997-03-15', '57');
INSERT INTO `student` VALUES (548, '56425', '谢红霞', '男', '2015-01-27', '57');
INSERT INTO `student` VALUES (549, '56426', '梁艳', '男', '1996-08-09', '57');
INSERT INTO `student` VALUES (550, '56427', '薛想', '女', '2007-11-03', '57');
INSERT INTO `student` VALUES (551, '56428', '郁慧', '男', '2016-07-09', '57');
INSERT INTO `student` VALUES (552, '56429', '罗强', '女', '2002-08-06', '57');
INSERT INTO `student` VALUES (553, '56430', '农凤英', '男', '1986-11-07', '57');
INSERT INTO `student` VALUES (554, '56431', '顾凯', '女', '2014-12-13', '57');
INSERT INTO `student` VALUES (555, '56432', '向文', '女', '2001-09-11', '57');
INSERT INTO `student` VALUES (556, '56433', '吴玉华', '女', '2014-01-03', '57');
INSERT INTO `student` VALUES (557, '56434', '张强', '女', '2017-08-07', '57');
INSERT INTO `student` VALUES (558, '56435', '张梅', '男', '1997-04-03', '57');
INSERT INTO `student` VALUES (559, '56436', '佘宇', '女', '2014-06-16', '57');
INSERT INTO `student` VALUES (560, '56437', '倪海燕', '男', '2020-12-01', '57');
INSERT INTO `student` VALUES (561, '56438', '杨春梅', '女', '1995-05-30', '57');
INSERT INTO `student` VALUES (562, '56439', '黄龙', '女', '1994-03-16', '57');
INSERT INTO `student` VALUES (563, '56440', '陶波', '女', '1996-11-11', '57');
INSERT INTO `student` VALUES (564, '56441', '陈杨', '女', '1972-11-16', '57');
INSERT INTO `student` VALUES (565, '56442', '谢东', '女', '1990-05-29', '57');
INSERT INTO `student` VALUES (566, '56443', '张浩', '男', '1993-11-04', '57');
INSERT INTO `student` VALUES (567, '56444', '张丹丹', '男', '2005-02-01', '57');
INSERT INTO `student` VALUES (568, '56445', '廖帅', '男', '1983-08-15', '57');
INSERT INTO `student` VALUES (569, '56446', '冯琳', '女', '1982-12-22', '57');
INSERT INTO `student` VALUES (570, '56447', '郭秀英', '女', '2007-01-17', '57');
INSERT INTO `student` VALUES (571, '56448', '丁淑华', '男', '1985-04-04', '57');
INSERT INTO `student` VALUES (572, '56449', '吴鹏', '男', '2009-02-01', '57');
INSERT INTO `student` VALUES (573, '56450', '张娟', '男', '1987-11-10', '57');
INSERT INTO `student` VALUES (574, '56451', '杨婷婷', '男', '1983-12-25', '57');
INSERT INTO `student` VALUES (575, '56452', '姜淑华', '女', '2017-03-29', '57');
INSERT INTO `student` VALUES (576, '56453', '王慧', '男', '2013-02-06', '57');
INSERT INTO `student` VALUES (577, '56454', '宋淑珍', '女', '2000-10-26', '57');
INSERT INTO `student` VALUES (578, '56455', '谭博', '女', '1972-07-21', '57');
INSERT INTO `student` VALUES (579, '56456', '朱建华', '男', '1978-10-29', '57');
INSERT INTO `student` VALUES (580, '56457', '胡云', '女', '1973-09-10', '57');
INSERT INTO `student` VALUES (581, '56458', '李波', '女', '1997-01-24', '57');
INSERT INTO `student` VALUES (582, '56459', '赵洋', '男', '1992-03-01', '57');
INSERT INTO `student` VALUES (583, '56460', '田秀梅', '女', '1978-11-25', '57');
INSERT INTO `student` VALUES (584, '56461', '余成', '女', '1975-09-04', '57');
INSERT INTO `student` VALUES (585, '56462', '陶波', '男', '2011-06-30', '57');
INSERT INTO `student` VALUES (586, '56463', '周岩', '男', '1985-06-12', '57');
INSERT INTO `student` VALUES (587, '56464', '陈华', '女', '1994-06-26', '57');
INSERT INTO `student` VALUES (588, '56465', '黄博', '女', '2008-11-06', '57');
INSERT INTO `student` VALUES (589, '56466', '赵慧', '男', '2010-09-02', '57');
INSERT INTO `student` VALUES (590, '56467', '姚坤', '女', '2011-04-23', '57');
INSERT INTO `student` VALUES (591, '56468', '萧洁', '男', '2013-02-22', '57');
INSERT INTO `student` VALUES (592, '56469', '王玉梅', '男', '1979-07-06', '57');
INSERT INTO `student` VALUES (593, '56470', '薛婷婷', '男', '2009-10-03', '57');
INSERT INTO `student` VALUES (594, '56471', '王华', '男', '1999-09-29', '57');
INSERT INTO `student` VALUES (595, '56472', '彭芳', '男', '2001-12-16', '57');
INSERT INTO `student` VALUES (596, '56473', '杨建国', '女', '2006-01-30', '57');
INSERT INTO `student` VALUES (597, '56474', '朱凤英', '女', '2001-12-11', '57');
INSERT INTO `student` VALUES (598, '56475', '赵桂英', '男', '2018-09-10', '57');
INSERT INTO `student` VALUES (599, '56476', '张涛', '男', '2008-07-28', '57');
INSERT INTO `student` VALUES (600, '56477', '栾琳', '女', '1987-08-01', '57');
INSERT INTO `student` VALUES (601, '56478', '李伟', '男', '1979-08-03', '57');
INSERT INTO `student` VALUES (602, '56479', '王萍', '女', '1998-01-14', '57');
INSERT INTO `student` VALUES (603, '56480', '郭坤', '女', '1984-08-20', '57');
INSERT INTO `student` VALUES (604, '56481', '郝桂芳', '男', '1978-12-13', '57');
INSERT INTO `student` VALUES (605, '56482', '郝红霞', '女', '1989-01-15', '57');
INSERT INTO `student` VALUES (606, '56483', '匡桂芝', '男', '1990-12-05', '57');
INSERT INTO `student` VALUES (607, '56484', '沈畅', '女', '2018-05-26', '57');
INSERT INTO `student` VALUES (608, '56485', '戴楠', '男', '1970-08-06', '57');
INSERT INTO `student` VALUES (609, '56486', '徐凯', '男', '1983-04-17', '57');
INSERT INTO `student` VALUES (610, '56487', '李东', '男', '1988-12-20', '57');
INSERT INTO `student` VALUES (611, '56488', '马博', '男', '2006-06-11', '57');
INSERT INTO `student` VALUES (612, '56489', '刘秀英', '男', '2013-02-18', '57');
INSERT INTO `student` VALUES (613, '56490', '张颖', '男', '2013-04-26', '57');
INSERT INTO `student` VALUES (614, '56491', '赵晨', '女', '1985-03-07', '57');
INSERT INTO `student` VALUES (615, '56492', '刘晶', '女', '2014-04-23', '57');
INSERT INTO `student` VALUES (616, '56493', '张晨', '男', '2004-07-17', '57');
INSERT INTO `student` VALUES (617, '56494', '郝芳', '男', '1986-09-10', '57');
INSERT INTO `student` VALUES (618, '56495', '张娜', '男', '1985-04-09', '57');
INSERT INTO `student` VALUES (619, '56496', '茹凤英', '男', '2018-05-11', '57');
INSERT INTO `student` VALUES (620, '56497', '刘飞', '女', '1988-01-16', '57');
INSERT INTO `student` VALUES (621, '56498', '赵瑞', '女', '2008-11-04', '57');
INSERT INTO `student` VALUES (622, '56499', '邹亮', '女', '2001-02-24', '57');
INSERT INTO `student` VALUES (623, '56500', '王鑫', '女', '1984-07-31', '57');
INSERT INTO `student` VALUES (624, '56501', '曹红霞', '男', '2009-06-11', '57');
INSERT INTO `student` VALUES (625, '56502', '连艳', '男', '1988-08-14', '57');
INSERT INTO `student` VALUES (626, '56503', '陶柳', '女', '1973-07-26', '57');
INSERT INTO `student` VALUES (627, '56504', '安博', '男', '1981-10-13', '57');
INSERT INTO `student` VALUES (628, '56505', '王晨', '男', '2002-12-10', '57');
INSERT INTO `student` VALUES (629, '56506', '杨婷婷', '女', '1975-03-13', '57');
INSERT INTO `student` VALUES (630, '56507', '罗琳', '男', '2010-02-20', '57');
INSERT INTO `student` VALUES (631, '56508', '时凤兰', '女', '1992-10-23', '57');
INSERT INTO `student` VALUES (632, '56509', '赖建平', '女', '2013-03-21', '57');
INSERT INTO `student` VALUES (633, '56510', '陈艳', '女', '1988-12-22', '57');
INSERT INTO `student` VALUES (634, '56511', '彭丽丽', '男', '1991-06-01', '57');
INSERT INTO `student` VALUES (635, '56512', '常建平', '女', '2007-01-01', '57');
INSERT INTO `student` VALUES (636, '56513', '梁秀云', '女', '1970-09-22', '57');
INSERT INTO `student` VALUES (637, '56514', '杨洋', '男', '1993-09-07', '57');
INSERT INTO `student` VALUES (638, '56515', '王彬', '男', '2017-05-11', '57');
INSERT INTO `student` VALUES (639, '56516', '胡飞', '女', '1975-09-20', '57');
INSERT INTO `student` VALUES (640, '56517', '陈慧', '女', '1993-07-21', '57');
INSERT INTO `student` VALUES (641, '56518', '苏秀荣', '男', '2010-12-09', '57');
INSERT INTO `student` VALUES (642, '56519', '杨春梅', '女', '2004-12-28', '57');
INSERT INTO `student` VALUES (643, '56520', '阮丽华', '男', '1986-05-16', '57');
INSERT INTO `student` VALUES (644, '56521', '魏华', '女', '2004-04-26', '57');
INSERT INTO `student` VALUES (645, '56522', '徐飞', '女', '1972-11-20', '57');
INSERT INTO `student` VALUES (646, '56523', '王芳', '男', '1995-06-16', '57');
INSERT INTO `student` VALUES (647, '56524', '王博', '男', '2018-06-19', '57');
INSERT INTO `student` VALUES (648, '56525', '刘丽娟', '女', '2007-07-08', '57');
INSERT INTO `student` VALUES (649, '56526', '彭海燕', '女', '1972-03-28', '57');
INSERT INTO `student` VALUES (650, '56527', '何琳', '女', '1992-05-11', '57');
INSERT INTO `student` VALUES (651, '56528', '梁琴', '女', '1978-05-18', '57');
INSERT INTO `student` VALUES (652, '56529', '史秀英', '男', '1971-05-26', '57');
INSERT INTO `student` VALUES (653, '56530', '郑欢', '男', '2016-03-07', '57');
INSERT INTO `student` VALUES (654, '56531', '张雪', '男', '2005-10-18', '57');
INSERT INTO `student` VALUES (655, '56532', '王龙', '女', '2020-05-21', '57');
INSERT INTO `student` VALUES (656, '56533', '吴娟', '男', '2005-04-19', '57');
INSERT INTO `student` VALUES (657, '56534', '胡婷', '女', '1983-10-17', '57');
INSERT INTO `student` VALUES (658, '56535', '王琳', '女', '2001-04-17', '57');
INSERT INTO `student` VALUES (659, '56536', '胡建国', '女', '2008-11-11', '57');
INSERT INTO `student` VALUES (660, '56537', '袁建', '男', '2018-07-26', '57');
INSERT INTO `student` VALUES (661, '56538', '杨桂珍', '女', '1973-07-20', '57');
INSERT INTO `student` VALUES (662, '56539', '韦云', '女', '1978-10-22', '57');
INSERT INTO `student` VALUES (663, '56540', '马东', '女', '1979-05-22', '57');
INSERT INTO `student` VALUES (664, '56541', '张燕', '男', '1986-06-09', '57');
INSERT INTO `student` VALUES (665, '56542', '杨明', '女', '2010-06-25', '57');
INSERT INTO `student` VALUES (666, '56543', '刘东', '女', '2013-04-13', '57');
INSERT INTO `student` VALUES (667, '56544', '何丹丹', '男', '2015-12-11', '57');
INSERT INTO `student` VALUES (668, '56545', '侯鑫', '女', '2009-09-14', '57');
INSERT INTO `student` VALUES (669, '56546', '杨彬', '女', '2021-01-07', '57');
INSERT INTO `student` VALUES (670, '56547', '权丽娟', '男', '1974-08-30', '57');
INSERT INTO `student` VALUES (671, '56548', '胡桂香', '男', '1974-01-15', '57');
INSERT INTO `student` VALUES (672, '56549', '夏辉', '女', '1989-03-03', '57');
INSERT INTO `student` VALUES (673, '56550', '杨彬', '女', '1988-12-15', '57');
INSERT INTO `student` VALUES (674, '56551', '李杰', '男', '2008-05-21', '57');
INSERT INTO `student` VALUES (675, '56552', '刘晨', '女', '1984-01-06', '57');
INSERT INTO `student` VALUES (676, '56553', '曹雪', '女', '1987-05-08', '57');
INSERT INTO `student` VALUES (677, '56554', '王桂珍', '女', '2019-03-18', '57');
INSERT INTO `student` VALUES (678, '56555', '鞠淑兰', '男', '2020-06-09', '57');
INSERT INTO `student` VALUES (679, '56556', '陶娜', '女', '1975-05-09', '57');
INSERT INTO `student` VALUES (680, '56557', '夏俊', '女', '2010-04-10', '57');
INSERT INTO `student` VALUES (681, '56558', '董淑兰', '女', '1999-04-15', '57');
INSERT INTO `student` VALUES (682, '56559', '洪秀英', '女', '1976-12-16', '57');
INSERT INTO `student` VALUES (683, '56560', '李兰英', '男', '1999-06-26', '57');
INSERT INTO `student` VALUES (684, '56561', '邵兰英', '女', '1997-04-30', '57');
INSERT INTO `student` VALUES (685, '56562', '熊兵', '女', '1987-12-31', '57');
INSERT INTO `student` VALUES (686, '56563', '韩东', '女', '1981-06-26', '57');
INSERT INTO `student` VALUES (687, '56564', '薛莹', '男', '2010-05-23', '57');
INSERT INTO `student` VALUES (688, '56565', '周霞', '男', '1983-11-14', '57');
INSERT INTO `student` VALUES (689, '56566', '王波', '男', '1978-02-16', '57');
INSERT INTO `student` VALUES (690, '56567', '朱琴', '女', '2006-02-16', '57');
INSERT INTO `student` VALUES (691, '56568', '赵楠', '女', '1985-11-19', '57');
INSERT INTO `student` VALUES (692, '56569', '侯欣', '男', '1975-08-21', '57');
INSERT INTO `student` VALUES (693, '56570', '顾雪梅', '女', '1995-06-01', '57');
INSERT INTO `student` VALUES (694, '56571', '杜帆', '男', '1973-02-01', '57');
INSERT INTO `student` VALUES (695, '56572', '吴莉', '女', '1980-05-18', '57');
INSERT INTO `student` VALUES (696, '56573', '周桂英', '女', '2010-02-08', '57');
INSERT INTO `student` VALUES (697, '56574', '孙秀梅', '女', '1992-02-24', '57');
INSERT INTO `student` VALUES (698, '56575', '邓凤英', '女', '2011-04-13', '57');
INSERT INTO `student` VALUES (699, '56576', '刘帅', '男', '2014-03-30', '57');
INSERT INTO `student` VALUES (700, '56577', '张娟', '女', '2000-06-14', '57');
INSERT INTO `student` VALUES (701, '56578', '王玉英', '男', '1971-10-24', '57');
INSERT INTO `student` VALUES (702, '56579', '韦桂花', '男', '1992-09-23', '57');
INSERT INTO `student` VALUES (703, '56580', '朱桂英', '男', '2014-05-16', '57');
INSERT INTO `student` VALUES (704, '56581', '崔娟', '男', '2008-12-29', '57');
INSERT INTO `student` VALUES (705, '56582', '赵海燕', '男', '1980-09-24', '57');
INSERT INTO `student` VALUES (706, '56583', '张秀英', '女', '2003-05-08', '57');
INSERT INTO `student` VALUES (707, '56584', '丁秀英', '男', '1973-07-16', '57');
INSERT INTO `student` VALUES (708, '56585', '马峰', '男', '2014-11-30', '57');
INSERT INTO `student` VALUES (709, '56586', '谭峰', '男', '1972-02-16', '57');
INSERT INTO `student` VALUES (710, '56587', '党娜', '男', '2014-01-28', '57');
INSERT INTO `student` VALUES (711, '56588', '李丽华', '女', '1992-07-09', '57');
INSERT INTO `student` VALUES (712, '56589', '康雪梅', '女', '1978-06-09', '57');
INSERT INTO `student` VALUES (713, '56590', '顾璐', '女', '1975-02-05', '57');
INSERT INTO `student` VALUES (714, '56591', '杨琳', '男', '2010-08-06', '57');
INSERT INTO `student` VALUES (715, '56592', '刘春梅', '男', '1983-04-01', '57');
INSERT INTO `student` VALUES (716, '56593', '尹淑珍', '男', '1986-10-18', '57');
INSERT INTO `student` VALUES (717, '56594', '姜淑珍', '男', '2018-09-08', '57');
INSERT INTO `student` VALUES (718, '56595', '谷建军', '女', '2019-05-29', '57');
INSERT INTO `student` VALUES (719, '56596', '张秀珍', '男', '1989-03-24', '57');
INSERT INTO `student` VALUES (720, '56597', '严秀英', '男', '1984-07-02', '57');
INSERT INTO `student` VALUES (721, '56598', '钟倩', '男', '2016-07-07', '57');
INSERT INTO `student` VALUES (722, '56599', '王建', '女', '1995-02-15', '57');
INSERT INTO `student` VALUES (723, '56600', '和海燕', '男', '2004-10-19', '57');
INSERT INTO `student` VALUES (724, '56601', '方波', '女', '1974-09-02', '57');
INSERT INTO `student` VALUES (725, '56602', '萧玉', '男', '2002-11-20', '57');
INSERT INTO `student` VALUES (726, '56603', '雷丽', '男', '1991-12-16', '57');
INSERT INTO `student` VALUES (727, '56604', '殷东', '女', '1979-01-27', '57');
INSERT INTO `student` VALUES (728, '56605', '王兰英', '男', '2012-05-12', '57');
INSERT INTO `student` VALUES (729, '56606', '朱红霞', '男', '1998-09-05', '57');
INSERT INTO `student` VALUES (730, '56607', '杨建国', '男', '1992-05-23', '57');
INSERT INTO `student` VALUES (731, '56608', '方伟', '男', '1996-06-05', '57');
INSERT INTO `student` VALUES (732, '56609', '袁凤兰', '男', '1973-12-18', '57');
INSERT INTO `student` VALUES (733, '56610', '张秀云', '女', '1980-03-07', '57');
INSERT INTO `student` VALUES (734, '56611', '周玉梅', '女', '1976-01-07', '57');
INSERT INTO `student` VALUES (735, '56612', '刘艳', '女', '2019-05-04', '57');
INSERT INTO `student` VALUES (736, '56613', '张帅', '男', '2020-11-25', '57');
INSERT INTO `student` VALUES (737, '56614', '钟秀兰', '女', '2011-09-09', '57');
INSERT INTO `student` VALUES (738, '56615', '周勇', '女', '1986-03-26', '57');
INSERT INTO `student` VALUES (739, '56616', '邵桂芝', '女', '2004-10-30', '57');
INSERT INTO `student` VALUES (740, '56617', '姚雪梅', '女', '1979-01-27', '57');
INSERT INTO `student` VALUES (741, '56618', '余桂芝', '男', '1970-03-01', '57');
INSERT INTO `student` VALUES (742, '56619', '申晶', '男', '2019-08-30', '57');
INSERT INTO `student` VALUES (743, '56620', '唐欢', '男', '2019-01-19', '57');
INSERT INTO `student` VALUES (744, '56621', '王芳', '女', '1973-04-04', '57');
INSERT INTO `student` VALUES (745, '56622', '李小红', '男', '1977-08-29', '57');
INSERT INTO `student` VALUES (746, '56623', '李娜', '男', '2009-04-16', '57');
INSERT INTO `student` VALUES (747, '56624', '谷超', '男', '1979-07-14', '57');
INSERT INTO `student` VALUES (748, '56625', '岳梅', '男', '2015-08-21', '57');
INSERT INTO `student` VALUES (749, '56626', '傅亮', '女', '2014-12-11', '57');
INSERT INTO `student` VALUES (750, '56627', '卢桂香', '女', '1987-08-27', '57');
INSERT INTO `student` VALUES (751, '56628', '李玉', '女', '2011-01-18', '57');
INSERT INTO `student` VALUES (752, '56629', '马婷婷', '男', '1992-10-27', '57');
INSERT INTO `student` VALUES (753, '56630', '焦鑫', '男', '1998-03-26', '57');
INSERT INTO `student` VALUES (754, '56631', '赖红', '女', '1998-10-02', '57');
INSERT INTO `student` VALUES (755, '56632', '温玉珍', '男', '1978-08-21', '57');
INSERT INTO `student` VALUES (756, '56633', '牛倩', '男', '1977-01-05', '57');
INSERT INTO `student` VALUES (757, '56634', '吴莹', '女', '2014-01-13', '57');
INSERT INTO `student` VALUES (758, '56635', '顾婷婷', '男', '2002-02-24', '57');
INSERT INTO `student` VALUES (759, '56636', '李博', '男', '2014-11-30', '57');
INSERT INTO `student` VALUES (760, '56637', '武莹', '男', '2003-08-14', '57');
INSERT INTO `student` VALUES (761, '56638', '陶利', '女', '1987-04-10', '57');
INSERT INTO `student` VALUES (762, '56639', '董春梅', '男', '1970-07-18', '57');
INSERT INTO `student` VALUES (763, '56640', '张明', '男', '1981-02-09', '57');
INSERT INTO `student` VALUES (764, '56641', '马桂荣', '男', '1996-10-09', '57');
INSERT INTO `student` VALUES (765, '56642', '徐兰英', '男', '1998-09-18', '57');
INSERT INTO `student` VALUES (766, '56643', '陈建', '男', '1977-05-01', '57');
INSERT INTO `student` VALUES (767, '56644', '陈龙', '男', '1973-09-17', '57');
INSERT INTO `student` VALUES (768, '56645', '王秀荣', '男', '2006-04-05', '57');
INSERT INTO `student` VALUES (769, '56646', '王秀珍', '女', '1998-01-03', '57');
INSERT INTO `student` VALUES (770, '56647', '姚玉珍', '女', '2005-05-27', '57');
INSERT INTO `student` VALUES (771, '56648', '王芳', '男', '2006-05-02', '57');
INSERT INTO `student` VALUES (772, '56649', '曹秀梅', '男', '1977-04-08', '57');
INSERT INTO `student` VALUES (773, '56650', '谭龙', '女', '1976-06-02', '57');
INSERT INTO `student` VALUES (774, '56651', '崔桂芳', '女', '2008-06-12', '57');
INSERT INTO `student` VALUES (775, '56652', '曾帅', '女', '2018-04-17', '57');
INSERT INTO `student` VALUES (776, '56653', '王勇', '男', '1978-06-20', '57');
INSERT INTO `student` VALUES (777, '56654', '孟建国', '男', '2009-11-06', '57');
INSERT INTO `student` VALUES (778, '56655', '李慧', '女', '1989-10-09', '57');
INSERT INTO `student` VALUES (779, '56656', '龚萍', '女', '1971-12-13', '57');
INSERT INTO `student` VALUES (780, '56657', '蔡凯', '男', '2018-10-30', '57');
INSERT INTO `student` VALUES (781, '56658', '杨梅', '女', '1992-12-20', '57');
INSERT INTO `student` VALUES (782, '56659', '谢鑫', '男', '1996-10-09', '57');
INSERT INTO `student` VALUES (783, '56660', '金杰', '男', '1979-09-02', '57');
INSERT INTO `student` VALUES (784, '56661', '李婷婷', '女', '1996-10-30', '57');
INSERT INTO `student` VALUES (785, '56662', '唐波', '女', '1985-01-06', '57');
INSERT INTO `student` VALUES (786, '56663', '袁伟', '男', '1986-01-30', '57');
INSERT INTO `student` VALUES (787, '56664', '谢艳', '女', '1975-11-18', '57');
INSERT INTO `student` VALUES (788, '56665', '罗波', '男', '1993-05-25', '57');
INSERT INTO `student` VALUES (789, '56666', '蒋瑞', '男', '1995-04-20', '57');
INSERT INTO `student` VALUES (790, '56667', '周建平', '女', '2000-03-16', '57');
INSERT INTO `student` VALUES (791, '56668', '张坤', '女', '2011-10-15', '57');
INSERT INTO `student` VALUES (792, '56669', '王兵', '女', '1993-09-07', '57');
INSERT INTO `student` VALUES (793, '56670', '卢玉', '男', '1988-10-14', '57');
INSERT INTO `student` VALUES (794, '56671', '姚俊', '男', '2001-01-03', '57');
INSERT INTO `student` VALUES (795, '56672', '李博', '男', '1996-09-08', '57');
INSERT INTO `student` VALUES (796, '56673', '王秀荣', '男', '1984-02-05', '57');
INSERT INTO `student` VALUES (797, '56674', '谭淑华', '男', '2007-06-21', '57');
INSERT INTO `student` VALUES (798, '56675', '刘璐', '女', '2013-06-29', '57');
INSERT INTO `student` VALUES (799, '56676', '邓建', '女', '1975-08-03', '57');
INSERT INTO `student` VALUES (800, '56677', '张英', '男', '2007-01-15', '57');
INSERT INTO `student` VALUES (801, '56678', '陈柳', '男', '2011-08-28', '57');
INSERT INTO `student` VALUES (802, '56679', '王利', '女', '1978-04-21', '57');
INSERT INTO `student` VALUES (803, '56680', '刘秀芳', '男', '1976-01-27', '57');
INSERT INTO `student` VALUES (804, '56681', '张淑兰', '女', '2010-02-14', '57');
INSERT INTO `student` VALUES (805, '56682', '谷阳', '女', '1976-06-21', '57');
INSERT INTO `student` VALUES (806, '56683', '杜雷', '男', '1973-07-29', '57');
INSERT INTO `student` VALUES (807, '56684', '张萍', '女', '2001-10-07', '57');
INSERT INTO `student` VALUES (808, '56685', '朱桂珍', '女', '2017-06-05', '57');
INSERT INTO `student` VALUES (809, '56686', '孙帅', '男', '2019-04-12', '57');
INSERT INTO `student` VALUES (810, '56687', '王兵', '女', '1990-03-21', '57');
INSERT INTO `student` VALUES (811, '56688', '谭丹', '女', '1996-09-30', '57');
INSERT INTO `student` VALUES (812, '56689', '戴冬梅', '男', '2002-02-27', '57');
INSERT INTO `student` VALUES (813, '56690', '万桂香', '女', '2012-03-25', '57');
INSERT INTO `student` VALUES (814, '56691', '周宁', '女', '1989-12-05', '57');
INSERT INTO `student` VALUES (815, '56692', '覃秀珍', '女', '1988-05-07', '57');
INSERT INTO `student` VALUES (816, '56693', '张淑珍', '女', '1998-10-29', '57');
INSERT INTO `student` VALUES (817, '56694', '金秀珍', '女', '1970-02-23', '57');
INSERT INTO `student` VALUES (818, '56695', '张楠', '男', '1991-04-23', '57');
INSERT INTO `student` VALUES (819, '56696', '杨晨', '男', '2008-09-10', '57');
INSERT INTO `student` VALUES (820, '56697', '江杨', '女', '2004-09-03', '57');
INSERT INTO `student` VALUES (821, '56698', '吴博', '男', '2017-08-24', '57');
INSERT INTO `student` VALUES (822, '56699', '王英', '男', '2003-04-04', '57');
INSERT INTO `student` VALUES (823, '56700', '戴芳', '男', '1993-10-19', '57');
INSERT INTO `student` VALUES (824, '56701', '刘冬梅', '男', '2009-07-16', '57');
INSERT INTO `student` VALUES (825, '56702', '刘明', '男', '1985-02-23', '57');
INSERT INTO `student` VALUES (826, '56703', '许峰', '男', '2019-06-22', '57');
INSERT INTO `student` VALUES (827, '56704', '梅兰英', '男', '1994-08-17', '57');
INSERT INTO `student` VALUES (828, '56705', '陈霞', '女', '1974-11-26', '57');
INSERT INTO `student` VALUES (829, '56706', '萧萍', '女', '1975-01-04', '57');
INSERT INTO `student` VALUES (830, '56707', '何莉', '男', '2003-04-19', '57');
INSERT INTO `student` VALUES (831, '56708', '林玉', '男', '1991-08-20', '57');
INSERT INTO `student` VALUES (832, '56709', '姜娟', '男', '2013-08-01', '57');
INSERT INTO `student` VALUES (833, '56710', '苏金凤', '女', '1994-02-28', '57');
INSERT INTO `student` VALUES (834, '56711', '彭丽丽', '男', '1978-03-18', '57');
INSERT INTO `student` VALUES (835, '56712', '杨建', '男', '1984-10-08', '57');
INSERT INTO `student` VALUES (836, '56713', '黄鑫', '男', '1976-11-29', '57');
INSERT INTO `student` VALUES (837, '56714', '徐浩', '女', '1986-02-05', '57');
INSERT INTO `student` VALUES (838, '56715', '陈秀兰', '女', '2002-09-08', '57');
INSERT INTO `student` VALUES (839, '56716', '杨秀梅', '女', '2017-12-14', '57');
INSERT INTO `student` VALUES (840, '56717', '文洋', '女', '2017-08-06', '57');
INSERT INTO `student` VALUES (841, '56718', '庄伟', '女', '1983-01-18', '57');
INSERT INTO `student` VALUES (842, '56719', '姚勇', '女', '2011-04-23', '57');
INSERT INTO `student` VALUES (843, '56720', '陈芳', '男', '1986-06-25', '57');
INSERT INTO `student` VALUES (844, '56721', '靳飞', '女', '2020-05-29', '57');
INSERT INTO `student` VALUES (845, '56722', '时桂英', '男', '2005-11-09', '57');
INSERT INTO `student` VALUES (846, '56723', '王华', '女', '2000-03-04', '57');
INSERT INTO `student` VALUES (847, '56724', '张博', '女', '1997-09-16', '57');
INSERT INTO `student` VALUES (848, '56725', '曾明', '男', '1981-09-02', '57');
INSERT INTO `student` VALUES (849, '56726', '贺桂芳', '男', '2012-09-16', '57');
INSERT INTO `student` VALUES (850, '56727', '赵倩', '男', '2019-02-20', '57');
INSERT INTO `student` VALUES (851, '56728', '王军', '男', '1999-10-22', '57');
INSERT INTO `student` VALUES (852, '56729', '罗洋', '男', '2000-08-27', '57');
INSERT INTO `student` VALUES (853, '56730', '宗玉梅', '男', '2018-01-06', '57');
INSERT INTO `student` VALUES (854, '56731', '唐亮', '女', '2005-05-20', '57');
INSERT INTO `student` VALUES (855, '56732', '齐芳', '女', '1988-04-20', '57');
INSERT INTO `student` VALUES (856, '56733', '杨鑫', '男', '2019-10-09', '57');
INSERT INTO `student` VALUES (857, '56734', '曲瑜', '男', '2008-06-16', '57');
INSERT INTO `student` VALUES (858, '56735', '张霞', '男', '1991-09-16', '57');
INSERT INTO `student` VALUES (859, '56736', '路峰', '女', '2013-10-21', '57');
INSERT INTO `student` VALUES (860, '56737', '张建军', '女', '2010-02-15', '57');
INSERT INTO `student` VALUES (861, '56738', '郑凤英', '女', '1978-05-19', '57');
INSERT INTO `student` VALUES (862, '56739', '李斌', '男', '2002-07-20', '57');
INSERT INTO `student` VALUES (863, '56740', '闻建军', '男', '1973-07-10', '57');
INSERT INTO `student` VALUES (864, '56741', '施平', '男', '1995-06-12', '57');
INSERT INTO `student` VALUES (865, '56742', '李刚', '男', '1975-08-24', '57');
INSERT INTO `student` VALUES (866, '56743', '王畅', '男', '1972-03-26', '57');
INSERT INTO `student` VALUES (867, '56744', '王英', '男', '2001-03-27', '57');
INSERT INTO `student` VALUES (868, '56745', '蒙霞', '女', '1974-11-13', '57');
INSERT INTO `student` VALUES (869, '56746', '张东', '男', '1973-07-27', '57');
INSERT INTO `student` VALUES (870, '56747', '赵健', '女', '1972-06-11', '57');
INSERT INTO `student` VALUES (871, '56748', '徐博', '男', '1998-12-09', '57');
INSERT INTO `student` VALUES (872, '56749', '王桂珍', '女', '1988-04-15', '57');
INSERT INTO `student` VALUES (873, '56750', '柯英', '男', '1972-01-08', '57');
INSERT INTO `student` VALUES (874, '56751', '李鑫', '男', '2007-01-30', '57');
INSERT INTO `student` VALUES (875, '56752', '刁婷婷', '男', '1975-10-26', '57');
INSERT INTO `student` VALUES (876, '56753', '杨斌', '男', '2006-11-23', '57');
INSERT INTO `student` VALUES (877, '56754', '刘雪', '男', '1997-01-31', '57');
INSERT INTO `student` VALUES (878, '56755', '王亮', '男', '1977-01-14', '57');
INSERT INTO `student` VALUES (879, '56756', '张秀云', '女', '1987-10-03', '57');
INSERT INTO `student` VALUES (880, '56757', '高超', '男', '1981-07-25', '57');
INSERT INTO `student` VALUES (881, '56758', '郑刚', '男', '2011-12-03', '57');
INSERT INTO `student` VALUES (882, '56759', '袁飞', '女', '2017-11-21', '57');
INSERT INTO `student` VALUES (883, '56760', '游英', '女', '1975-09-10', '57');
INSERT INTO `student` VALUES (884, '56761', '白辉', '男', '2005-03-30', '57');
INSERT INTO `student` VALUES (885, '56762', '梁婷婷', '女', '1991-05-18', '57');
INSERT INTO `student` VALUES (886, '56763', '方玉', '男', '1983-07-09', '57');
INSERT INTO `student` VALUES (887, '56764', '刘瑞', '男', '1987-03-10', '57');
INSERT INTO `student` VALUES (888, '56765', '李秀梅', '男', '2009-11-10', '57');
INSERT INTO `student` VALUES (889, '56766', '谢建', '女', '1978-02-08', '57');
INSERT INTO `student` VALUES (890, '56767', '邹莹', '女', '2016-03-17', '57');
INSERT INTO `student` VALUES (891, '56768', '梁玉梅', '女', '1988-12-13', '57');
INSERT INTO `student` VALUES (892, '56769', '张冬梅', '女', '2011-11-06', '57');
INSERT INTO `student` VALUES (893, '56770', '翟娟', '女', '1981-04-16', '57');
INSERT INTO `student` VALUES (894, '56771', '杨婷婷', '男', '1995-07-02', '57');
INSERT INTO `student` VALUES (895, '56772', '尹兵', '女', '1999-11-19', '57');
INSERT INTO `student` VALUES (896, '56773', '邓倩', '女', '1999-02-05', '57');
INSERT INTO `student` VALUES (897, '56774', '廖玉兰', '女', '1998-05-05', '57');
INSERT INTO `student` VALUES (898, '56775', '桂楠', '男', '1980-10-24', '57');
INSERT INTO `student` VALUES (899, '56776', '乔岩', '男', '1973-07-16', '57');
INSERT INTO `student` VALUES (900, '56777', '张阳', '男', '2020-12-06', '57');
INSERT INTO `student` VALUES (901, '56778', '刘勇', '女', '2017-06-14', '57');
INSERT INTO `student` VALUES (902, '56779', '吴英', '男', '2014-04-22', '57');
INSERT INTO `student` VALUES (903, '56780', '薛玉', '女', '1982-08-01', '57');
INSERT INTO `student` VALUES (904, '56781', '余波', '女', '1993-02-15', '57');
INSERT INTO `student` VALUES (905, '56782', '李佳', '男', '2006-11-27', '57');
INSERT INTO `student` VALUES (906, '56783', '赵坤', '男', '2008-06-14', '57');
INSERT INTO `student` VALUES (907, '56784', '彭娜', '女', '1984-06-30', '57');
INSERT INTO `student` VALUES (908, '56785', '芦小红', '女', '1993-08-10', '57');
INSERT INTO `student` VALUES (909, '56786', '郭杨', '男', '1973-09-23', '57');
INSERT INTO `student` VALUES (910, '56787', '施斌', '男', '2019-06-26', '57');
INSERT INTO `student` VALUES (911, '56788', '夏辉', '女', '1970-08-22', '57');
INSERT INTO `student` VALUES (912, '56789', '丁亮', '女', '2002-05-01', '57');
INSERT INTO `student` VALUES (913, '56790', '刘淑珍', '女', '1993-09-11', '57');
INSERT INTO `student` VALUES (914, '56791', '张丽华', '女', '2005-12-01', '57');
INSERT INTO `student` VALUES (915, '56792', '魏丽华', '男', '2018-06-14', '57');
INSERT INTO `student` VALUES (916, '56793', '许想', '男', '1981-07-02', '57');
INSERT INTO `student` VALUES (917, '56794', '范洁', '男', '1991-01-17', '57');
INSERT INTO `student` VALUES (918, '56795', '赵丹', '男', '2014-05-11', '57');
INSERT INTO `student` VALUES (919, '56796', '杨娟', '男', '1981-01-26', '57');
INSERT INTO `student` VALUES (920, '56797', '娄玉', '女', '1980-06-04', '57');
INSERT INTO `student` VALUES (921, '56798', '赵娜', '男', '2015-10-27', '57');
INSERT INTO `student` VALUES (922, '56799', '袁秀华', '女', '2012-11-19', '57');
INSERT INTO `student` VALUES (923, '56800', '范莉', '男', '1986-03-01', '57');
INSERT INTO `student` VALUES (924, '56801', '龙海燕', '女', '1981-09-14', '57');
INSERT INTO `student` VALUES (925, '56802', '汪芳', '男', '2016-02-15', '57');
INSERT INTO `student` VALUES (926, '56803', '张佳', '男', '1977-12-08', '57');
INSERT INTO `student` VALUES (927, '56804', '胡桂香', '女', '1974-08-13', '57');
INSERT INTO `student` VALUES (928, '56805', '翁兵', '男', '1983-01-20', '57');
INSERT INTO `student` VALUES (929, '56806', '张洁', '女', '1987-08-13', '57');
INSERT INTO `student` VALUES (930, '56807', '黄岩', '男', '2014-05-28', '57');
INSERT INTO `student` VALUES (931, '56808', '吕淑珍', '男', '2020-08-07', '57');
INSERT INTO `student` VALUES (932, '56809', '舒婷', '男', '2020-01-02', '57');
INSERT INTO `student` VALUES (933, '56810', '陆雪梅', '男', '2009-03-30', '57');
INSERT INTO `student` VALUES (934, '56811', '乔婷', '男', '1999-02-26', '57');
INSERT INTO `student` VALUES (935, '56812', '郑波', '男', '2020-09-21', '57');
INSERT INTO `student` VALUES (936, '56813', '姜玉英', '女', '2003-03-27', '57');
INSERT INTO `student` VALUES (937, '56814', '张萍', '男', '1981-09-14', '57');
INSERT INTO `student` VALUES (938, '56815', '雷宁', '女', '1983-12-20', '57');
INSERT INTO `student` VALUES (939, '56816', '张燕', '男', '1992-11-29', '57');
INSERT INTO `student` VALUES (940, '56817', '魏坤', '男', '1983-01-28', '57');
INSERT INTO `student` VALUES (941, '56818', '周霞', '男', '2015-10-28', '57');
INSERT INTO `student` VALUES (942, '56819', '许丹丹', '男', '1997-09-12', '57');
INSERT INTO `student` VALUES (943, '56820', '谢玉英', '男', '2020-05-12', '57');
INSERT INTO `student` VALUES (944, '56821', '周瑜', '女', '1992-02-26', '57');
INSERT INTO `student` VALUES (945, '56822', '李超', '女', '1986-05-13', '57');
INSERT INTO `student` VALUES (946, '56823', '朱坤', '男', '1992-02-11', '57');
INSERT INTO `student` VALUES (947, '56824', '杜宇', '女', '1977-06-06', '57');
INSERT INTO `student` VALUES (948, '56825', '李洁', '男', '1981-09-29', '57');
INSERT INTO `student` VALUES (949, '56826', '赵金凤', '男', '2009-11-10', '57');
INSERT INTO `student` VALUES (950, '56827', '钱飞', '女', '1980-02-02', '57');
INSERT INTO `student` VALUES (951, '56828', '张佳', '男', '2009-08-08', '57');
INSERT INTO `student` VALUES (952, '56829', '张鹏', '男', '1991-03-05', '57');
INSERT INTO `student` VALUES (953, '56830', '张丹', '女', '1999-04-14', '57');
INSERT INTO `student` VALUES (954, '56831', '苗玉华', '女', '2009-07-11', '57');
INSERT INTO `student` VALUES (955, '56832', '刘燕', '女', '2018-08-01', '57');
INSERT INTO `student` VALUES (956, '56833', '岳秀荣', '男', '1993-03-31', '57');
INSERT INTO `student` VALUES (957, '56834', '严晶', '男', '1978-09-16', '57');
INSERT INTO `student` VALUES (958, '56835', '叶丹丹', '男', '1976-05-02', '57');
INSERT INTO `student` VALUES (959, '56836', '胡丽娟', '女', '1981-02-19', '57');
INSERT INTO `student` VALUES (960, '56837', '黄丹丹', '女', '1970-04-19', '57');
INSERT INTO `student` VALUES (961, '56838', '李金凤', '女', '1972-06-27', '57');
INSERT INTO `student` VALUES (962, '56839', '张梅', '男', '2000-04-22', '57');
INSERT INTO `student` VALUES (963, '56840', '古萍', '女', '1982-05-20', '57');
INSERT INTO `student` VALUES (964, '56841', '毛柳', '男', '1992-02-19', '57');
INSERT INTO `student` VALUES (965, '56842', '罗佳', '男', '2002-06-15', '57');
INSERT INTO `student` VALUES (966, '56843', '郑兰英', '女', '1978-01-28', '57');
INSERT INTO `student` VALUES (967, '56844', '梁欢', '女', '2011-10-02', '57');
INSERT INTO `student` VALUES (968, '56845', '林帅', '男', '1987-05-03', '57');
INSERT INTO `student` VALUES (969, '56846', '孙玉华', '男', '2013-07-04', '57');
INSERT INTO `student` VALUES (970, '56847', '王丹丹', '男', '1995-10-06', '57');
INSERT INTO `student` VALUES (971, '56848', '严玉', '男', '1986-07-09', '57');
INSERT INTO `student` VALUES (972, '56849', '田伟', '男', '1995-08-03', '57');
INSERT INTO `student` VALUES (973, '56850', '解涛', '男', '2002-02-25', '57');
INSERT INTO `student` VALUES (974, '56851', '杜想', '女', '1982-03-05', '57');
INSERT INTO `student` VALUES (975, '56852', '陈桂英', '男', '2002-07-19', '57');
INSERT INTO `student` VALUES (976, '56853', '潘丽', '男', '1992-10-06', '57');
INSERT INTO `student` VALUES (977, '56854', '袁雪', '女', '2003-04-17', '57');
INSERT INTO `student` VALUES (978, '56855', '杨志强', '女', '1988-04-28', '57');
INSERT INTO `student` VALUES (979, '56856', '兰林', '女', '2002-10-18', '57');
INSERT INTO `student` VALUES (980, '56857', '李荣', '女', '1996-08-28', '57');
INSERT INTO `student` VALUES (981, '56858', '陈洁', '男', '1979-04-20', '57');
INSERT INTO `student` VALUES (982, '56859', '傅桂荣', '男', '2011-08-07', '57');
INSERT INTO `student` VALUES (983, '56860', '梁桂兰', '男', '1976-11-22', '57');
INSERT INTO `student` VALUES (984, '56861', '王淑英', '男', '1988-01-01', '57');
INSERT INTO `student` VALUES (985, '56862', '王桂兰', '女', '1993-03-11', '57');
INSERT INTO `student` VALUES (986, '56863', '黄玲', '女', '1997-12-31', '57');
INSERT INTO `student` VALUES (987, '56864', '张畅', '女', '1985-09-29', '57');
INSERT INTO `student` VALUES (988, '56865', '冉敏', '男', '2017-03-24', '57');
INSERT INTO `student` VALUES (989, '56866', '马璐', '男', '1989-10-23', '57');
INSERT INTO `student` VALUES (990, '56867', '殷红霞', '男', '1985-05-23', '57');
INSERT INTO `student` VALUES (991, '56868', '石欣', '女', '1990-12-29', '57');
INSERT INTO `student` VALUES (992, '56869', '余雪', '女', '1997-08-17', '57');
INSERT INTO `student` VALUES (993, '56870', '邓玲', '女', '2021-05-31', '57');
INSERT INTO `student` VALUES (994, '56871', '胡玉华', '男', '1978-10-04', '57');
INSERT INTO `student` VALUES (995, '56872', '陈伟', '男', '1994-11-02', '57');
INSERT INTO `student` VALUES (996, '56873', '王敏', '男', '1986-09-04', '57');
INSERT INTO `student` VALUES (997, '56874', '苏丽华', '男', '2016-06-12', '57');
INSERT INTO `student` VALUES (998, '56875', '王桂芝', '男', '1996-02-22', '57');
INSERT INTO `student` VALUES (999, '56876', '赖晶', '男', '1995-06-08', '57');

-- ----------------------------
-- Table structure for vendors
-- ----------------------------
DROP TABLE IF EXISTS `vendors`;
CREATE TABLE `vendors`  (
  `vend_id` int(11) NOT NULL AUTO_INCREMENT,
  `vend_name` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `vend_address` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vend_city` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vend_state` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vend_zip` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `vend_country` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`vend_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1007 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of vendors
-- ----------------------------
INSERT INTO `vendors` VALUES (1001, 'Anvils R Us', '123 Main Street', 'Southfield', 'MI', '48075', 'USA');
INSERT INTO `vendors` VALUES (1002, 'LT Supplies', '500 Park Street', 'Anytown', 'OH', '44333', 'USA');
INSERT INTO `vendors` VALUES (1003, 'ACME', '555 High Street', 'Los Angeles', 'CA', '90046', 'USA');
INSERT INTO `vendors` VALUES (1004, 'Furball Inc.', '1000 5th Avenue', 'New York', 'NY', '11111', 'USA');
INSERT INTO `vendors` VALUES (1005, 'Jet Set', '42 Galaxy Road', 'London', NULL, 'N16 6PS', 'England');
INSERT INTO `vendors` VALUES (1006, 'Jouets Et Ours', '1 Rue Amusement', 'Paris', NULL, '45678', 'France');

-- ----------------------------
-- Procedure structure for isAdult
-- ----------------------------
DROP PROCEDURE IF EXISTS `isAdult`;
delimiter ;;
CREATE PROCEDURE `isAdult`()
BEGIN

	SELECT *,
	if( YEAR(CURDATE())-YEAR(birthday)>=18, "成年人", "未成年") as 是否成年
	FROM student;


END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
